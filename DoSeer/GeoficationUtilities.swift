//
//  GeoficationUtilities.swift
//  DoSeer
//
//  Created by Bastiaan on 12/08/2015.
//  Copyright (c) 2015 Bastiaan. All rights reserved.
//

import UIKit
import MapKit
import CoreData

var geotifications = [Geotification]()

// MARK: Helper Functions

/*
class AppHelper {
    
    class var locationManager : CLLocationManager {
        struct Static {
            static let locationManager2 : CLLocationManager = CLLocationManager()
        }
        return Static.locationManager2
    }
}*/

func showSimpleAlertWithTitle(title: String!, #message: String, #viewController: UIViewController) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .Alert)
    let action = UIAlertAction(title: "OK", style: .Cancel, handler: nil)
    alert.addAction(action)
    viewController.presentViewController(alert, animated: true, completion: nil)
}

func zoomToUserLocationInMapView(mapView: MKMapView) {
    if let coordinate = mapView.userLocation.location?.coordinate {
        let region = MKCoordinateRegionMakeWithDistance(coordinate, 10000, 10000)
        mapView.setRegion(region, animated: true)
    }
}

var GlobalMainQueue: dispatch_queue_t {
    return dispatch_get_main_queue()
}

func regionWithGeotification(geotification: Geotification) -> CLCircularRegion {
    // 1
    let region = CLCircularRegion(center: geotification.coordinate, radius: geotification.radius, identifier: geotification.identifier)
    // 2
    region.notifyOnEntry = true //region.notifyOnEntry
    region.notifyOnExit = false //!region.notifyOnEntry
    return region
}

func startMonitoringGeotification(geotification: Geotification, viewController: UIViewController, locationManager: CLLocationManager) {
    // 1
    if !CLLocationManager.isMonitoringAvailableForClass(CLCircularRegion) {
        showSimpleAlertWithTitle("Error", message: "Geofencing is not supported on this device!", viewController: viewController)
        return
    }
    // 2
    if CLLocationManager.authorizationStatus() != .AuthorizedAlways {
        //showSimpleAlertWithTitle("Warning", message: "Your geotification is saved but will only be activated once you grant this app permission to access the device location.", viewController: self)
    }
    // 3
    let region = regionWithGeotification(geotification)
    // 4
    locationManager.startMonitoringForRegion(region)
}

func stopMonitoringGeotification(geotification: Geotification, locationManager: CLLocationManager) {
    
    for region in locationManager.monitoredRegions {
        if let circularRegion = region as? CLCircularRegion {
            if circularRegion.identifier == geotification.identifier {
                locationManager.stopMonitoringForRegion(circularRegion)
            }
        }
    }
}

func stopMonitoringAllRegions(locationManager: CLLocationManager) {
    for region in locationManager.monitoredRegions {
        let mregion: CLRegion = region as! CLRegion
        locationManager.stopMonitoringForRegion(mregion)
    }
}

func loadAllGeotifications(viewController: UIViewController, locationManager: CLLocationManager) {
    geotifications = []
    
    // Load all contexts
    
    var contexts = [NSManagedObject]()
    
    //1
    let appDelegate =
    UIApplication.sharedApplication().delegate as! AppDelegate
    
    let managedContext = appDelegate.managedObjectContext!
    
    //2
    let fetchRequest = NSFetchRequest(entityName:"Context")
    
    let sortDescriptor1 = NSSortDescriptor(key: "name", ascending: true, selector: "caseInsensitiveCompare:")
    fetchRequest.sortDescriptors = [sortDescriptor1]
    
    let predicate = NSPredicate(format: "(trash == false OR trash == nil)")
    
    // Set the predicate on the fetch request
    fetchRequest.predicate = predicate
    
    //3
    var error: NSError?
    
    let fetchedResults =
    managedContext.executeFetchRequest(fetchRequest,
        error: &error) as! [NSManagedObject]?
    
    if let results = fetchedResults {
        contexts = results
    } else {
        println("Could not fetch \(error), \(error!.userInfo)")
    }
    
    for context in contexts
    {
        var loclat : Double = 0.0
        var loclong : Double = 0.0
        if let lat = context.valueForKey("locationlat") as? Double {
            loclat = lat
        }
        
        if let long = context.valueForKey("locationlong") as? Double {
            loclong = long
        }
        
        if(loclat != 0 && loclong != 0) {
            var coordinate: CLLocationCoordinate2D = CLLocationCoordinate2D()
            coordinate.latitude = loclat
            coordinate.longitude = loclong
            var radius: CLLocationDistance = CLLocationDistance()
            radius.distanceTo(1000)
            var geotification  = Geotification(coordinate: coordinate, radius: radius, identifier: context.valueForKey("name") as! String, contextIdentifier: context.valueForKey("id") as! String)
            startMonitoringGeotification(geotification, viewController, locationManager)
            geotifications.append(geotification)
            
            /*
            let circularRegion = CLCircularRegion(center: coordinate, radius: radius, identifier: "currentregion")
            
            if circularRegion.containsCoordinate(currentLocation) {
            println("yes context found!")
            }
            */
        }
    }
}


