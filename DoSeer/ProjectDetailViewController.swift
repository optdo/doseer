//
//  ProjectDetailViewController.swift
//  next3
//
//  Created by Bastiaan on 25/02/2015.
//  Copyright (c) 2015 Bastiaan. All rights reserved.
//

import UIKit
import CoreData

protocol ProjectDetailDelegate{
    func deleteProject(controller:ProjectDetailViewController)
}

class ProjectDetailViewController: UITableViewController {

    var delegate:ProjectDetailDelegate! = nil
    var project : NSManagedObject!
    var timeslotid: String!
    var completed: Bool! = false
    
    private var datePickerHidden = false

    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var timeslotLabel: UILabel!
    @IBOutlet var typeLabel: UILabel!
    @IBOutlet var dueDateLabel: UILabel!
    @IBOutlet var doneButton: UIBarButtonItem!
    
    @IBOutlet var completedSegmentedControl: UISegmentedControl!
    @IBOutlet var sharedSegmentedControl: UISegmentedControl!
    @IBOutlet var dueDatePicker: UIDatePicker!
    
    @IBAction func cancelToProjectDetailViewController(segue:UIStoryboardSegue) {
    }
    
    @IBAction func nameFieldEditingChanged(sender: UITextField) {
        if (!sender.text.isEmpty) {
            doneButton.enabled = true
        }
        else {
            doneButton.enabled = false
        }
    }
    
    
    @IBAction func saveTimeslotSelection(segue:UIStoryboardSegue) {
        let selectorViewController = segue.sourceViewController as! TimeslotSelectorViewController
        if let selectedItem = selectorViewController.selectedItem {
            timeslotLabel.text = selectedItem
            timeslotid = selectorViewController.selectedItemId
//            println(timeslotid)
        }
    }

    func deleteProject(sender: UIBarButtonItem)
    {
        delegate.deleteProject(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //didChangeDate()
        
        toggleDatePicker()

        if (project != nil)
        {
            var toolBarItems = NSMutableArray()
            
            //        var systemButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Bookmarks, target: nil, action: nil)
            //        toolBarItems.addObject(systemButton1)
            
            var systemButton2 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
            toolBarItems.addObject(systemButton2)
            
            var systemButton3 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Trash, target: self, action: "deleteProject:")
            toolBarItems.addObject(systemButton3)
            
            var systemButton4 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
            toolBarItems.addObject(systemButton4)
            
            //var systemButton5 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Compose, target: nil, action: nil)
            //toolBarItems.addObject(systemButton5)
            
            self.navigationController?.toolbarHidden = false
            self.setToolbarItems(toolBarItems as [AnyObject], animated: true)            
            
            self.nameTextField.text = project.valueForKey("name") as! String?
            completed = project.valueForKey("completed") as! Bool?

            if completed == nil || completed  == false {
                completedSegmentedControl.selectedSegmentIndex = 1
            }
            else {
                completedSegmentedControl.selectedSegmentIndex = 0
            }
            
            /*
            if project.valueForKey("duedate") != nil
            {
                self.dueDatePicker.date = project.valueForKey("duedate") as! NSDate
                self.dueDateLabel.text = NSDateFormatter.localizedStringFromDate(self.dueDatePicker.date, dateStyle: .ShortStyle, timeStyle: .ShortStyle)
            }
            else
            {
                self.dueDateLabel.text = "Not set"
            }*/
            if(project.valueForKey("shared") as! Bool) {
                self.sharedSegmentedControl.selectedSegmentIndex = 0
            }
            timeslotid = project.valueForKey("timeslotid") as! String?
            println(timeslotid)

            if(timeslotid != nil)
            {
            
                //1
                let appDelegate =
                UIApplication.sharedApplication().delegate as! AppDelegate
                
                let managedContext = appDelegate.managedObjectContext!
                
                //2
                let fetchRequest = NSFetchRequest(entityName:"Timeslot")
                
                //3
                var error: NSError?
            
            let predicate = NSPredicate(format: "id == %@", timeslotid)
            
            // Set the predicate on the fetch request
            fetchRequest.predicate = predicate
            
                    var items = [NSManagedObject]()
                
                let fetchedResults =
                managedContext.executeFetchRequest(fetchRequest,
                    error: &error) as! [NSManagedObject]?
                
                if let results = fetchedResults {
                    items = results
                } else {
                    println("Could not fetch \(error), \(error!.userInfo)")
                }
                if (items.count==1)
                {
                    self.timeslotLabel.text = items[0].valueForKey("name") as! String?
                }
            }
            doneButton.enabled = true
        }
        else
        {
            self.timeslotLabel.text = "Select";
            self.dueDateLabel.text = "Not set";
        }

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        return 4
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch (indexPath.section, indexPath.row) {
        case (0, 3):
            toggleDatePicker()
        default:
            ()
        }
    }

    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        println(indexPath.row)
        println(indexPath.section)
        if datePickerHidden && indexPath.section == 0 && indexPath.row == 4 {
            return 0
        } else {
            return super.tableView(tableView, heightForRowAtIndexPath: indexPath)
        }
    }
    
    private func toggleDatePicker() {
        datePickerHidden = !datePickerHidden
        
        // Force table to update its contents
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
    @IBAction
    func didChangeDate() {
        dueDateLabel.text = NSDateFormatter.localizedStringFromDate(dueDatePicker.date, dateStyle: .ShortStyle, timeStyle: .ShortStyle)
    }
    
    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as UITableViewCell

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        if segue.identifier == "ShowTimeslotSelector" {
            let itemViewController = segue.destinationViewController as! TimeslotSelectorViewController
            itemViewController.selectedItemId = timeslotid
        }
    }
}
