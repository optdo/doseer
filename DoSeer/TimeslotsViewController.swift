//
//  TimeslotsViewController.swift
//  next3
//
//  Created by Bastiaan on 25/02/2015.
//  Copyright (c) 2015 Bastiaan. All rights reserved.
//

import UIKit
import CoreData

class TimeslotsViewController: UITableViewController, TimeslotDetailDelegate{
    
    let undoer = NSUndoManager()
    override var undoManager : NSUndoManager {
        get {
            return self.undoer
        }
    }
    
    var filter: Bool = true
    var items = [NSManagedObject]()

    @IBOutlet var filterBarButton: UIBarButtonItem!
    @IBAction func cancelToTimeslotsViewController(segue:UIStoryboardSegue) {
    }
    
    func toggleFilter(sender: UIBarButtonItem)
    {
        if(filter) {
            filter = false
            sender.title = "Filter Off"
        }
        else {
            filter = true
            sender.title = "Filter On"
        }
        self.tableView.reloadData()
    }
    
    @IBAction func saveTimeslotDetail(segue:UIStoryboardSegue) {
        
        let detailViewController = segue.sourceViewController as! TimeslotDetailViewController
        
        //1
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext!
        
        if(detailViewController.timeslot == nil)
        {
            //2
            let entity =  NSEntityDescription.entityForName("Timeslot",
                inManagedObjectContext:
                managedContext)
            
            let item = NSManagedObject(entity: entity!,
                insertIntoManagedObjectContext:managedContext)
            
            //3
            item.setValue(detailViewController.nameTextField.text, forKey: "name")
            var uuid = NSUUID().UUIDString
            item.setValue(uuid, forKey: "id")
            item.setValue(false, forKey: "trash")
            
            //4
            var error: NSError?
            if !managedContext.save(&error) {
                println("Could not save \(error), \(error?.userInfo)")
            }
            //5

            items.append(item)
        }
        else
        {
            let item = detailViewController.timeslot;
            
            item.setValue(detailViewController.nameTextField.text, forKey: "name")
            
            var error: NSError?
            if !managedContext.save(&error) {
                println("Could not save \(error), \(error?.userInfo)")
            }
            
            // Updated view
//            loadItems()
        }
//        self.tableView.reloadData()
    }
    
    func deleteTimeslot(controller: TimeslotDetailViewController) {
        
        deleteItem(controller.timeslot)
        
        self.tableView.reloadData()
        
        controller.navigationController?.popViewControllerAnimated(true)
    }
    
    func deleteItem(itemToDelete:NSManagedObject)
    {
        undoManager.registerUndoWithTarget(self, selector:Selector("deleteItem:"), object:itemToDelete)
        undoManager.setActionName("delete timeslot")
        
        // Delete it from the managedObjectContext
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext!
        
        if self.undoer.undoing {
            itemToDelete.setValue(false, forKey: "trash")
        }
        else
        {
            itemToDelete.setValue(true, forKey: "trash")
        }
        
        var error: NSError?
        if !managedContext.save(&error) {
            println("Could not save \(error), \(error?.userInfo)")
        }
        // TODO: Update all actions to remove project id
        
        // Refresh the table view to indicate that it's deleted
        self.loadItems()
        
        if self.undoer.undoing || self.undoer.redoing {
            self.tableView.reloadData()
        }
    }
    
    override func canBecomeFirstResponder() -> Bool {
        return true
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        becomeFirstResponder()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        resignFirstResponder()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.filterBarButton.action = Selector("toggleFilter:")
        
        let font = UIFont.systemFontOfSize(11.0)
        font
        filterBarButton.setTitleTextAttributes([NSFontAttributeName: font, NSForegroundColorAttributeName: UIColor.blueColor()],forState: UIControlState.Normal)
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        loadItems()
        self.tableView.reloadData()
    }
    
    func loadItems() {
        //1
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext!
        
        //2
        let fetchRequest = NSFetchRequest(entityName:"Timeslot")

        let predicate = NSPredicate(format: "(trash == false OR trash == nil)")
        
        // Set the predicate on the fetch request
        fetchRequest.predicate = predicate
        
        let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        
        // Set the list of sort descriptors in the fetch request,
        // so it includes the sort descriptor
        fetchRequest.sortDescriptors = [sortDescriptor]

        //3
        var error: NSError?
        
        let fetchedResults =
        managedContext.executeFetchRequest(fetchRequest,
            error: &error) as! [NSManagedObject]?
        
        if let results = fetchedResults {
            items = results
        } else {
            println("Could not fetch \(error), \(error!.userInfo)")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        return items.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("TimeslotCell", forIndexPath: indexPath) as! UITableViewCell
    
    // Configure the cell...
    let item = items[indexPath.row]
    cell.textLabel!.text = item.valueForKey("name") as! String?
    
        if ( item.valueForKey("totalactions") != nil)
        {
            
            let totalactions = item.valueForKey("totalactions") as! Double
            let totalcost = item.valueForKey("totalcosts") as! Double
            let totaltime = item.valueForKey("totaltime") as! NSTimeInterval?
            
            let totalactionsex = item.valueForKey("totalactionsex") as! Double
            let totalcostex = item.valueForKey("totalcostsex") as! Double
            let totaltimeex = item.valueForKey("totaltimeex") as! NSTimeInterval?
            
            var timetext : String? = ""
            if totaltime != nil{
                let dcf = NSDateComponentsFormatter()
                dcf.zeroFormattingBehavior = .Pad
                dcf.allowedUnits = .CalendarUnitHour | .CalendarUnitMinute
                timetext = dcf.stringFromTimeInterval(totaltime!)
            }
            
            var timetextex : String? = ""
            if totaltime != nil{
                let dcf = NSDateComponentsFormatter()
                dcf.zeroFormattingBehavior = .Pad
                dcf.allowedUnits = .CalendarUnitHour | .CalendarUnitMinute
                timetextex = dcf.stringFromTimeInterval(totaltimeex!)
            }
            
            if totalactions > 0
            {
                var formatter = NSNumberFormatter()
                formatter.numberStyle = .CurrencyStyle
                
//                cell.detailTextLabel!.text = NSString(format: "%.0f/%.0f actions | %@/%@ | %@/%@", totalactionsex, totalactions, timetextex!, timetext!, formatter.stringFromNumber(totalcostex)!, formatter.stringFromNumber(totalcost)!) as String

                if filter {
                    cell.detailTextLabel!.text = NSString(format: "%.0f actions | %@ | %@", totalactionsex, timetextex!, formatter.stringFromNumber(totalcostex)!) as String
                }
                else {
                    cell.detailTextLabel!.text = NSString(format: "%.0f actions | %@ | %@", totalactions, timetext!, formatter.stringFromNumber(totalcost)!) as String
                }
//                cell.detailTextLabel!.text = NSString(format: "%.0f/%.0f actions | %@/%@ | $%.2f/$%.2f ", totalactionsex, totalactions, timetextex!, timetext!, totalcostex, totalcost) as String
            }
            else {
                cell.detailTextLabel!.text = "No actions"
            }
        }
        else {
            cell.detailTextLabel!.text = "Not updated"
        }
        
    return cell
    }
    
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    
    override func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [AnyObject]? {
        
        let deleteClosure = { (action: UITableViewRowAction!, indexPath: NSIndexPath!) -> Void in
            let itemToDelete = self.items[indexPath.row]
            
            self.deleteItem(itemToDelete)
            
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        }
        
        let editClosure = { (action: UITableViewRowAction!, indexPath: NSIndexPath!) -> Void in
            self.performSegueWithIdentifier("ShowTimeslotDetail",sender:indexPath)
        }
        
        let deleteAction = UITableViewRowAction(style: .Default, title: "Delete", handler: deleteClosure)
        let editAction = UITableViewRowAction(style: .Normal, title: "Edit", handler: editClosure)
        
        return [deleteAction, editAction]
    }
    
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            
            let itemToDelete = items[indexPath.row]
            
            // Delete it from the managedObjectContext
            let appDelegate =
            UIApplication.sharedApplication().delegate as! AppDelegate
            let managedContext = appDelegate.managedObjectContext!
            managedContext.deleteObject(itemToDelete)
            
            var error: NSError?
            if !managedContext.save(&error) {
                println("Could not save \(error), \(error?.userInfo)")
            }
            
            // Refresh the table view to indicate that it's deleted
            loadItems()
            
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        if segue.identifier == "ShowTimeslotDetail" {
            var indexPath = sender as! (NSIndexPath)
            var item = items[indexPath.row]
            let vc = segue.destinationViewController as! TimeslotDetailViewController
            vc.timeslot = item
            vc.title = "Timeslot"
            vc.delegate = self
        }
        if segue.identifier == "ShowActionsFromTimeslots" {
            var indexPath: NSIndexPath! = self.tableView.indexPathForSelectedRow()
            var item = items[indexPath.row]
            let vc = segue.destinationViewController as! ActionsViewController
            vc.timeslotId = item.valueForKey("id") as! String
//            vc.timeslot = item
        }
    }


}
