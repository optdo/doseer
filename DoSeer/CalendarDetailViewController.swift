//
//  CalendarDetailViewController.swift
//  next3
//
//  Created by Bastiaan on 25/02/2015.
//  Copyright (c) 2015 Bastiaan. All rights reserved.
//

import UIKit
import EventKit
import CoreData

protocol CalendarDetailDelegate{
    func deleteCalendar(controller:CalendarDetailViewController)
}

class CalendarDetailViewController: UITableViewController {
    
    let eventStore = EKEventStore()


    var delegate:CalendarDetailDelegate! = nil
    
    var calendar : NSManagedObject!
    var calendarid: String! = nil

    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var doneButton: UIBarButtonItem!
    
    @IBOutlet var typeLabel: UILabel!
    @IBOutlet var providerLabel: UILabel!
    @IBOutlet var urlTextField: UITextField!
    @IBOutlet var AccountIDTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    
    @IBOutlet var calendarNameLabel: UILabel!
    
    @IBAction func cancelToCalendarDetailViewController(segue:UIStoryboardSegue) {
    }
    
    @IBAction func saveCalendarTypeSelection(segue:UIStoryboardSegue) {
        let selectorViewController = segue.sourceViewController as! CalendarTypeSelectorViewController
        if let selectedItem = selectorViewController.selectedItem {
            typeLabel.text = selectedItem
        }
    }
    
    @IBAction func saveCalendarSelection(segue:UIStoryboardSegue) {
        let selectorViewController = segue.sourceViewController as! CalendarSelectorViewController
        if let selectedItem = selectorViewController.selectedItem {
            calendarNameLabel.text = selectedItem
            calendarid = selectorViewController.selectedItemId
        }
    }
    
    func deleteCalendar(sender: UIBarButtonItem)
    {
        delegate.deleteCalendar(self)
    }
    
    @IBAction func changedName(sender: UITextField) {
        if (!sender.text.isEmpty) {
            doneButton.enabled = true
        }
        else {
            doneButton.enabled = false  
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (calendar != nil)
        {
            var toolBarItems = NSMutableArray()
            
            //        var systemButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Bookmarks, target: nil, action: nil)
            //        toolBarItems.addObject(systemButton1)
            
            var systemButton2 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
            toolBarItems.addObject(systemButton2)
            
            var systemButton3 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Trash, target: self, action: "deleteCalendar:")
            toolBarItems.addObject(systemButton3)
            
            var systemButton4 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
            toolBarItems.addObject(systemButton4)
            
            //var systemButton5 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Compose, target: nil, action: nil)
            //toolBarItems.addObject(systemButton5)
            
            self.navigationController?.toolbarHidden = false
            self.setToolbarItems(toolBarItems as [AnyObject], animated: true)
            
            self.nameTextField.text = calendar.valueForKey("name") as! String?
            self.typeLabel.text = calendar.valueForKey("type") as! String?
            calendarid = calendar.valueForKey("externalid") as! String?
            
            if calendarid != nil {
                checkCalendarAuthorizationStatus()
/*                if let c4 = eventStore.calendarWithIdentifier(calendarid) as EKCalendar? {
                    calendarNameLabel.text = c4.title
                }*/
            }
            doneButton.enabled = true
        }

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        return 3
    }

    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as UITableViewCell

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    func checkCalendarAuthorizationStatus() {
        let status = EKEventStore.authorizationStatusForEntityType(EKEntityTypeEvent)
        
        switch (status) {
        case EKAuthorizationStatus.NotDetermined:
            // This happens on first-run
            requestAccessToCalendar()
        case EKAuthorizationStatus.Authorized:
            // Things are in line with being able to show the calendars in the table view
            self.loadCalendarName()
//            refreshTableView()
//        case EKAuthorizationStatus.Restricted, EKAuthorizationStatus.Denied:
            // We need to help them give us permission
//            needPermissionView.fadeIn()
        default:
            let alert = UIAlertView(title: "Privacy Warning", message: "You have not granted permission for this app to access your Calendar", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
    }
    
    func requestAccessToCalendar() {
        eventStore.requestAccessToEntityType(EKEntityTypeEvent, completion: {
            (accessGranted: Bool, error: NSError!) in
            
            if accessGranted == true {
                dispatch_async(dispatch_get_main_queue(), {
                    self.loadCalendarName()
//                    self.refreshTableView()
                })
            } else {
                dispatch_async(dispatch_get_main_queue(), {
//                    self.needPermissionView.fadeIn()
                })
            }
        })
    }
    
    func loadCalendarName() {
        
        let allCalendars : [EKCalendar] = self.eventStore.calendarsForEntityType(EKEntityTypeEvent) as! [EKCalendar]
        
        let calendars = allCalendars.filter { $0.calendarIdentifier == self.calendarid }
        
        if let c = calendars.first {
            calendarNameLabel.text = c.title
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        if segue.identifier == "ShowCalendarTypeSelector" {
            let itemViewController = segue.destinationViewController as! CalendarTypeSelectorViewController
            if typeLabel.text != "Select" {
                itemViewController.selectedItem = typeLabel.text
                if (typeLabel.text == "Appointments") {
                    itemViewController.selectedItem = "Appointments"
                    itemViewController.selectedItemIndex = 0
                }
                else {
                    itemViewController.selectedItem = "Timeslots"
                    itemViewController.selectedItemIndex = 1
                }
                
            }            
            else {
                itemViewController.selectedItem = "Timeslots"
                itemViewController.selectedItemIndex = 1
            }
        }
        else if segue.identifier == "ShowCalendarSelector" {
            if calendarNameLabel.text != "Select" {
                let itemViewController = segue.destinationViewController as! CalendarSelectorViewController
                itemViewController.selectedItem = calendarNameLabel.text
                itemViewController.selectedItemId = calendarid
            }
        }
    }


}
