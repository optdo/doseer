//
//  RepeatViewController.swift
//  next3.1.2
//
//  Created by Bastiaan on 13/04/2015.
//  Copyright (c) 2015 Bastiaan. All rights reserved.
//

import UIKit

class RepeatViewController: UITableViewController, UITextFieldDelegate, UIPickerViewDelegate {

    @IBOutlet var frequencyLabel: UILabel!    
    @IBOutlet var everyLabel: UILabel!
    @IBOutlet var frequencyPicker: UIPickerView!
    @IBOutlet var everyPicker: UIPickerView!
    var repeatFrequency : Int! = 0
    var repeatEvery : Int! = 1
    
     var elements:Array<String>?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
         elements = ["Never","Weekly","Monthly","Yearly"]
        
        frequencyPicker.tag = 0
        everyPicker.tag = 1
        
        frequencyPicker.delegate = self
        everyPicker.delegate = self
        
        switch (repeatFrequency) {
        case (1):
            frequencyLabel.text = "Daily"
            //frequencyPicker.selectRow(2, inComponent: PickerComponent.size.toRaw(), animated: false)
        case (2):
            frequencyLabel.text = "Weekly"
            frequencyPicker.selectRow(1, inComponent: 0, animated: false)
        case (3):
            frequencyLabel.text = "Monthly"
            frequencyPicker.selectRow(2, inComponent: 0, animated: false)
        case (4):
            frequencyLabel.text = "Yearly"
            frequencyPicker.selectRow(3, inComponent: 0, animated: false)
        default:
            frequencyLabel.text = "Never"
            everyPicker.hidden = true
            everyLabel.hidden = true
            // hide row
        }
        everyLabel.text = String(repeatEvery)
        everyPicker.selectRow(repeatEvery, inComponent: 0, animated: false)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return 4
    }

    
    func numberOfComponentsInPickerView(pickerView: UIPickerView!) -> Int
    {
        return 1;
    }
    
    func pickerView(pickerView: UIPickerView!, numberOfRowsInComponent component: Int) -> Int
    {
        if pickerView.tag == 0 {
            return elements!.count;
        }
        if pickerView.tag == 1 {
            return 50;
        }
        else {
            return 0
        }
    }
    
    func pickerView(pickerView: UIPickerView!, titleForRow row: Int, forComponent component: Int) -> String
    {
        if pickerView.tag == 0 {
            return elements![row]
        }
        if pickerView.tag == 1 {
            return NSString(format: "%d", row + 1) as String
        }
        return ""
    }
    
    func pickerView(pickerView: UIPickerView!, didSelectRow row: Int, inComponent component: Int)
    {
        if pickerView.tag == 0 {
            frequencyLabel.text = elements![row]
            if row>0 {
                everyPicker.hidden = false
                everyLabel.hidden = false
                // unhide row
            }
            else {
                everyPicker.hidden = true
                everyLabel.hidden = true
                // hide row
            }
            
            }
        if pickerView.tag == 1 {
            everyLabel.text = NSString(format: "%d", row + 1) as String
        }
    }
    
    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as! UITableViewCell

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

}
