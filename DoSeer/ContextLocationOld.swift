//
//  ContextLocationViewController.swift
//  DoSer
//
//  Created by Bastiaan on 28/07/2015.
//  Copyright (c) 2015 Bastiaan. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class ContextLocationViewControllerOld: UIViewController, CLLocationManagerDelegate, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UISearchDisplayDelegate {
    
    @IBOutlet var curLongLabel: UILabel!
    @IBOutlet var currentLatLabel: UILabel!
    
    @IBOutlet var longTextField: UITextField!
    @IBOutlet var latTextField: UITextField!
    
    @IBOutlet var mapView: MKMapView!
    
    var mapItems : [MKMapItem] = [MKMapItem]()
    
    var curLat : Double = 0.0
    var curLong : Double = 0.0
    var locLat : Double = 0.0
    var locLong : Double = 0.0
    
    var locationManager: CLLocationManager = CLLocationManager()
    var startLocation: CLLocation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        startLocation = nil
        
        if(locLat != 0 && locLong != 0) {
            latTextField.text = String(format: "%.4f", locLat)
            longTextField.text = String(format: "%.4f",locLong)
        }
    }
    
    @IBAction func updateLocation(sender: AnyObject) {
        
        longTextField.text = curLongLabel.text
        latTextField.text = currentLatLabel.text
        locLat = curLat
        locLong = curLong
    }
    
    @IBAction func clearLocation(sender: AnyObject) {
        longTextField.text = ""
        latTextField.text = ""
        locLat = 0.0
        locLong = 0.0
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!)
    {
        var latestLocation = locations[locations.count - 1] as! CLLocation
        
        curLat = latestLocation.coordinate.latitude
        curLong = latestLocation.coordinate.longitude
        
        currentLatLabel.text = String(format: "%.4f", curLat)
        curLongLabel.text = String(format: "%.4f",curLong)
    }
    
    func locationManager(manager: CLLocationManager!,
        didFailWithError error: NSError!) {
            
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // to do
        
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("LocationSelectorCell") as! UITableViewCell
        
        /*
        if let calendars = self.calendars {
        let calendarName = calendars[indexPath.row].title
        cell.textLabel?.text = calendarName
        if calendars[indexPath.row].calendarIdentifier == selectedItemId {
        cell.accessoryType = .Checkmark
        } else {
        cell.accessoryType = .None
        }
        } else {
        cell.textLabel?.text = "Unknown Calendar Name"
        }
        */
        
        return cell
    }
    
    func searchDisplayController(controller: UISearchDisplayController!, shouldReloadTableForSearchString searchString: String!) -> Bool {
        //self.filterContentForSearchText(searchString)
        lookupLocation()
        return true
    }
    
    func searchDisplayController(controller: UISearchDisplayController!, shouldReloadTableForSearchScope searchOption: Int) -> Bool {
        //self.filterContentForSearchText(self.searchDisplayController!.searchBar.text)
        lookupLocation()
        return true
    }
    
    func lookupLocation() {
        var searchController:UISearchController!
        var annotation:MKAnnotation!
        var localSearchRequest:MKLocalSearchRequest!
        var localSearch:MKLocalSearch!
        var localSearchResponse:MKLocalSearchResponse!
        var error:NSError!
        
        //1
        self.searchDisplayController!.searchBar.resignFirstResponder()
        dismissViewControllerAnimated(true, completion: nil)
        if self.mapView.annotations.count != 0 {
            annotation = self.mapView.annotations[0] as! MKAnnotation
            self.mapView.removeAnnotation(annotation)
        }
        //2
        localSearchRequest = MKLocalSearchRequest()
        localSearchRequest.naturalLanguageQuery = self.searchDisplayController!.searchBar.text
        localSearch = MKLocalSearch(request: localSearchRequest)
        localSearch.startWithCompletionHandler { (localSearchResponse, error) -> Void in
            
            if localSearchResponse == nil{
                var alert = UIAlertView(title: nil, message: "Place not found", delegate: self, cancelButtonTitle: "Try again")
                alert.show()
                return
            }
            //3
            
            for item in localSearchResponse.mapItems as! [MKMapItem] {
                println("Name = \(item.name)")
                println("Phone = \(item.phoneNumber)")
            }
            
            var pointAnnotation:MKPointAnnotation!
            var pinAnnotationView:MKPinAnnotationView!
            
            pointAnnotation = MKPointAnnotation()
            pointAnnotation.title = self.searchDisplayController!.searchBar.text
            pointAnnotation.coordinate = CLLocationCoordinate2D(latitude: localSearchResponse.boundingRegion.center.latitude, longitude:     localSearchResponse.boundingRegion.center.longitude)
            
            
            pinAnnotationView = MKPinAnnotationView(annotation: pointAnnotation, reuseIdentifier: nil)
            self.mapView.centerCoordinate = pointAnnotation.coordinate
            self.mapView.addAnnotation(pinAnnotationView.annotation)
        }
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
