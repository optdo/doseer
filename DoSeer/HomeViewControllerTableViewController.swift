//
//  HomeViewControllerTableViewController.swift
//  next3
//
//  Created by Bastiaan on 25/02/2015.
//  Copyright (c) 2015 Bastiaan. All rights reserved.
//

import UIKit
import EventKit
import CoreData

class HomeViewControllerTableViewController: UITableViewController, CLLocationManagerDelegate {
    
    let locationManager = CLLocationManager()
    var currentLocation : CLLocationCoordinate2D = CLLocationCoordinate2D()
    
    @IBOutlet var barButtonItem: UIBarButtonItem!
    
    @IBOutlet var nowLabel: UILabel!
    @IBOutlet var inboxLabel: UILabel!
    
    
    var currentTimeslotName : String = "NA"
    var currentContextName : String = "NA"
    var currentTimeslotId: String = ""
    var currentContextId : String = ""
    var statusButton: UIButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        
        //        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        
        
        /*
        // Clean up
        
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext!
        
        var dataString = "April 1, 2030" as String
        var dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy"
        dateFormatter.timeZone = NSTimeZone.localTimeZone()
        
        // convert string into date
        let dateValue = dateFormatter.dateFromString(dataString) as NSDate!
        
        let predicate = NSPredicate(format: "startdate > %@", dateValue)
        
        let fetchRequest = NSFetchRequest(entityName: "Action")
        fetchRequest.predicate = predicate
        
        let items = managedContext.executeFetchRequest(fetchRequest, error: nil) as! [NSObject]
        
        for item in items {
        println(item.valueForKey("name"))
        item.setValue(nil, forKey: "startdate")
        item.setValue(nil, forKey: "duedate")
        // ... Update additional properties with new values
        }
        managedContext.save(nil)
        */
        
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        
        // Get timeslot
        updateTimeslot()
        
        // Get contexts and set geotifications
        loadAllGeotifications(self, locationManager)

        // Configure status label
        
/*
        let font = UIFont.systemFontOfSize(11.0)
        font
        barButtonItem.tintColor = UIColor.blackColor()
        
        barButtonItem.setTitleTextAttributes([NSFontAttributeName: font, NSForegroundColorAttributeName: UIColor.grayColor()],forState: UIControlState.Normal)
*/
        
        statusButton.titleLabel?.numberOfLines = 2
//        statusButton.setTitle("test\ntest", forState: UIControlState.Normal)
        statusButton.setTitleColor(UIColor.grayColor(), forState: UIControlState.Normal)
        statusButton.titleLabel!.font =  UIFont.systemFontOfSize(11.0)
        statusButton.titleLabel!.textAlignment = .Center
        statusButton.sizeToFit()
//        button.targetForAction("actioncall", withSender: nil)
        barButtonItem.customView = statusButton
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        updateTotals()
    }
    
    func updateTotals() {
        let nowActions = loadActionItems("now", currentTimeslotId, currentContextId, "", true, false)
        
        nowLabel.text = String(format: "%d", nowActions.count)
        
        let inboxActions = loadActionItems("inbox", "", "", "", true, false)
        
        inboxLabel.text = String(format: "%d", inboxActions.count)
    }
    
    func updateTimeslot() {
        
        currentTimeslotName = "NA"
        
        //1
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext!
        
        //2
        let fetchRequest = NSFetchRequest(entityName:"Calendar")
        
        let sortDescriptor1 = NSSortDescriptor(key: "name", ascending: true, selector: "caseInsensitiveCompare:")
        fetchRequest.sortDescriptors = [sortDescriptor1]
        
        let predicate = NSPredicate(format: "type == 'Timeslots' AND (trash == false OR trash == nil)")
        //let predicate = NSPredicate(format: "(trash == false OR trash == nil)")
        
        // Set the predicate on the fetch request
        fetchRequest.predicate = predicate
        
        //3
        var error: NSError?
        
        let fetchedResults =
        managedContext.executeFetchRequest(fetchRequest,
            error: &error) as! [NSManagedObject]?
        
        var calendarid : String? = nil
        
        if let results = fetchedResults {
            let item = results.first
            if (item != nil) {
                println(item!.valueForKey("name"))
                calendarid = item!.valueForKey("externalid") as! String?
                
                var eventCalendars: [EKCalendar] = [EKCalendar]()
                
                var eventStore : EKEventStore = EKEventStore()
                // 'EKEntityTypeReminder' or 'EKEntityTypeEvent'
                eventStore.requestAccessToEntityType(EKEntityTypeEvent, completion: {
                    granted, error in
                    if (granted) && (error == nil) {
                        println("granted \(granted)")
                        println("error  \(error)")
                    }
                })
                
                let allCalendars : [EKCalendar] = eventStore.calendarsForEntityType(EKEntityTypeEvent) as! [EKCalendar]
                
                let calendars = allCalendars.filter { $0.calendarIdentifier == calendarid }
                
                if let c = calendars.first {
                    eventCalendars.append(c)
                }
                
                // What about Calendar entries?
                var startDate=NSDate() //.dateByAddingTimeInterval(-60*60*24)
                var endDate=NSDate() //.dateByAddingTimeInterval(60*60*24*3)
                var predicate2 = eventStore.predicateForEventsWithStartDate(startDate, endDate: endDate, calendars: eventCalendars)
                
                println("startDate:\(startDate) endDate:\(endDate)")
                var eV = eventStore.eventsMatchingPredicate(predicate2) as! [EKEvent]!
                
                if eV != nil {
                    for i in eV {
                        /*
                        println("Title  \(i.title)" )
                        println("stareDate: \(i.startDate)" )
                        println("endDate: \(i.endDate)" )
                        */
                        currentTimeslotName = i.title
                        lookupTimeslotId()
                        displayCurrentStatus()
                    }
                }
            }
        } else {
            println("Could not fetch \(error), \(error!.userInfo)")
        }
        
        let popTime = dispatch_time(DISPATCH_TIME_NOW,
            Int64(10 * Double(NSEC_PER_SEC))) // 1
        dispatch_after(popTime, GlobalMainQueue) {
            self.locationManager.startUpdatingLocation()
            self.updateTimeslot()
        }
    }
    
    func displayCurrentStatus() {
        locationManager.stopUpdatingLocation()
        statusButton.setTitle(String(format: "Current Timeslot: %@\nCurrent Context: %@", currentTimeslotName, currentContextName), forState: UIControlState.Normal)
        statusButton.sizeToFit()
    }
    
    
    func locationManager(manager: CLLocationManager!, monitoringDidFailForRegion region: CLRegion!, withError error: NSError!) {
        println("Monitoring failed for region with identifier: \(region.identifier)")
    }
    
    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
        println("Location Manager failed with the following error: \(error)")
    }
    
    func lookupTimeslotId() {
        
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext!
        
        let fetchRequest = NSFetchRequest(entityName:"Timeslot")
        let predicate = NSPredicate(format: "name == %@", currentTimeslotName)
        
        fetchRequest.predicate = predicate
        
        var error: NSError?
        
        let fetchedResults =
        managedContext.executeFetchRequest(fetchRequest,
            error: &error) as! [NSManagedObject]?
        
        var projects = [NSManagedObject]()
        
        if let results = fetchedResults {
            
            projects = results
            if results.count>0 {
                currentTimeslotId = projects.first?.valueForKey("id") as! String
                return
            }
            
        } else {
            println("Could not fetch \(error), \(error!.userInfo)")
        }
        
        // Not found
        currentTimeslotName = currentTimeslotName + " (not matched)"
        currentTimeslotId = ""
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!)
    {
        var latestLocation = locations[locations.count - 1] as! CLLocation
        
        currentLocation = latestLocation.coordinate
        
        var contextfound : Bool = false
        
        for geotification in geotifications {
            let circularRegion = CLCircularRegion(center: geotification.coordinate, radius: 100.0, identifier: "currentregion")
            
            if circularRegion.containsCoordinate(currentLocation) {
                println(NSDate())
                println(geotification.identifier)
                
                currentContextName = geotification.identifier
                contextfound = true
                currentContextId = geotification.contextId
                break
            }
        }
        if !contextfound {
            currentContextName = "NA"
            currentContextId = ""
        }
        displayCurrentStatus()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        return 8
    }
    
    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as UITableViewCell
    
    // Configure the cell...
    
    return cell
    }
    */
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the specified item to be editable.
    return true
    }
    */
    
    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    if editingStyle == .Delete {
    // Delete the row from the data source
    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    } else if editingStyle == .Insert {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
    }
    */
    
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
    
    }
    */
    
    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the item to be re-orderable.
    return true
    }
    */
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        if segue.identifier == "ShowInbox" {
            let vc = segue.destinationViewController as! ActionsViewController
            vc.mode = "inbox"
        }
        else if segue.identifier == "ShowActions" {
            let vc = segue.destinationViewController as! ActionsViewController
        }
        else if segue.identifier == "ShowNow" {
            let vc = segue.destinationViewController as! ActionsViewController
            vc.mode = "now"
            vc.timeslotId = currentTimeslotId
            vc.contextId = currentContextId
        }
    }
    
    
}
