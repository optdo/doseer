//
//  ActionsViewController.swift
//  next3
//
//  Created by Bastiaan on 25/02/2015.
//  Copyright (c) 2015 Bastiaan. All rights reserved.
//

import UIKit
import CoreData

class ActionsViewController: UITableViewController, UISearchResultsUpdating, UISearchBarDelegate, ActionDetailDelegate {
    
    let undoer = NSUndoManager()
    override var undoManager : NSUndoManager {
        get {
            return self.undoer
        }
    }
    
    var items = [NSManagedObject]()
//    var project : NSManagedObject!
//    var context : NSManagedObject!
//    var timeslot : NSManagedObject!
    var timeslotId : String = ""
    var contextId : String = ""
    var projectId : String = ""
    var mode : String = ""
    var completed: Bool = false
    var filter: Bool = true
    var search: Bool = false
    var filteredTableData = [NSManagedObject]()
    var resultSearchController = UISearchController()
    private var totalTime : NSTimeInterval = 0
    private var totalCost : Double = 0.0
    private var statusButton: UIButton = UIButton()

    
    @IBOutlet var actionTableView: UITableView!
    //@IBOutlet var actionSearchBar: UISearchBar!
    @IBAction func cancelToActionsViewController(segue:UIStoryboardSegue) {
    }
    
    func toggleCompleted(sender: UIBarButtonItem)
    {
        if(completed) {
            completed = false
            sender.title = "Show Completed"
//            sender.tintColor = UIColor.grayColor()
        }
        else {
            completed = true
            sender.title = "Hide Completed"
//            sender.tintColor = UIColor.blueColor()
        }
        
        loadItems()
        self.reloadTableData()
    }
    
    func toggleFilter(sender: UIBarButtonItem)
    {
        if(filter) {
            filter = false
            
            sender.title = "Filter Off"
//            sender.tintColor = UIColor.grayColor()
        }
        else {
            filter = true

            sender.title = "Filter On"
//ƒ            sender.tintColor = UIColor.blueColor()
        }
        
        loadItems()
        self.reloadTableData()
    }
    
    func deleteAction(controller: ActionDetailViewController) {
        
        deleteItem(controller.action)
        
        self.reloadTableData()
        
        controller.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func saveActionDetail(segue:UIStoryboardSegue) {
        
        var completed : Bool = false
        let detailViewController = segue.sourceViewController as! ActionDetailViewController
        
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext:NSManagedObjectContext = appDelegate.managedObjectContext!
        
        var item: NSManagedObject! = nil
        
        if(detailViewController.action == nil)
        {
            //2
            let entity =  NSEntityDescription.entityForName("Action",
                inManagedObjectContext:
                managedContext)
            
            item = NSManagedObject(entity: entity!,
                insertIntoManagedObjectContext:managedContext)
            
            item.setValue(false, forKey: "completed")
            item.setValue(false, forKey: "trash")
            item.setValue(NSDate(), forKey: "creationdate")
            
            var uuid = NSUUID().UUIDString
            
            item.setValue(uuid, forKey: "id")
        }
        else
        {
            item = detailViewController.action;
        }
        
        item.setValue(detailViewController.nameTextField.text, forKey: "name")
        item.setValue(detailViewController.projectid, forKey: "projectid")
        item.setValue(detailViewController.contextid, forKey: "contextid")
        if(detailViewController.startdate != nil) {
            item.setValue(detailViewController.startdate, forKey: "startdate")
        }
        if(detailViewController.duedate != nil) {
            item.setValue(detailViewController.duedate, forKey: "duedate")
        }
        if(detailViewController.timeestimate != nil) {
            item.setValue(detailViewController.timeestimate, forKey: "timeestimate")
        }
        
        if(detailViewController.costTextField.text != nil)  && (detailViewController.costTextField.text != "") {
            item.setValue((detailViewController.costTextField.text as NSString).floatValue, forKey: "cost")
        }
        var priority: NSNumber
        switch (detailViewController.prioritySegmentedControl.selectedSegmentIndex) {
        case (0):
            priority = 3
        case (1):
            priority = 2
        case (2):
            priority = 1
        case (3):
            priority = 0
        default:
            priority = 2
        }
        var completeditem = item.valueForKey("completed") as! Bool!
        if completeditem == nil {
            completeditem = false
        }
        
        item.setValue(priority, forKey: "priority")
        if detailViewController.notesTextView.textColor == UIColor.blackColor() {
            item.setValue(detailViewController.notesTextView.text, forKey: "notes")
        }
        item.setValue(NSDate(), forKey: "modifieddate")
        
        item.setValue(detailViewController.repeatFrequency, forKey: "repeatfrequency")
        item.setValue(detailViewController.repeatEvery, forKey: "repeatevery")
        
        if (detailViewController.completedSegmentedControl.selectedSegmentIndex == 1 && completeditem == true)
        {
            item.setValue(false, forKey: "completed")
            item.setValue(nil, forKey: "completeddate")
        }
        
        if (detailViewController.completedSegmentedControl.selectedSegmentIndex == 0 && completeditem == false)
        {
            completed = true
        }
        
        self.saveAction(item, completed: completed, oldprojectid: detailViewController.oldprojectid, oldcontextid: detailViewController.oldcontextid)
        
    }
    
    func saveAction(item: NSManagedObject!, completed : Bool, oldprojectid : String?, oldcontextid : String?) {
        
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext:NSManagedObjectContext = appDelegate.managedObjectContext!
        
        if (completed)
        {
            item.setValue(true, forKey: "completed")
            item.setValue(NSDate(), forKey: "completeddate")
        }
        
        // Check for repeating action
        var repeatEvery : Int = 1
        var repeatFrequency : Int = 0
        
        if (item.valueForKey("repeatfrequency") != nil)
        {
            repeatEvery = item.valueForKey("repeatevery") as! Int
            repeatFrequency = item.valueForKey("repeatfrequency") as! Int
        }
        
        if completed && repeatFrequency>0 {
            
            let entity =  NSEntityDescription.entityForName("Action",
                inManagedObjectContext:
                managedContext)
            
            var newItem = NSManagedObject(entity: entity!,
                insertIntoManagedObjectContext:managedContext)
            
            let attributes = newItem.entity.attributesByName as! [String: NSAttributeDescription]
            for attributeName in attributes.keys  {
                
                var value: AnyObject? = item.valueForKey(attributeName)
                if value != nil {
                    newItem.setValue(value, forKey: attributeName)
                }
            }
            
            var existingduedate : NSDate = item.valueForKey("duedate") as! NSDate
            var existingstartdate : NSDate? = nil
            if (item.valueForKey("startdate") != nil)
            {
                existingstartdate = item.valueForKey("startdate") as! NSDate?
            }
            
            
            var duedate : NSDate = NSDate()
            var startdate : NSDate = NSDate()
            switch (repeatFrequency) {
            case (1):
                var components = NSDateComponents()
                components.setValue(repeatEvery, forComponent: NSCalendarUnit.CalendarUnitDay);
                duedate = NSCalendar.currentCalendar().dateByAddingComponents(components, toDate: duedate, options: NSCalendarOptions(0))!
            case (2):
                var components = NSDateComponents()
                components.setValue(repeatEvery, forComponent: NSCalendarUnit.CalendarUnitWeekOfYear);
                duedate = NSCalendar.currentCalendar().dateByAddingComponents(components, toDate: duedate, options: NSCalendarOptions(0))!
            case (3):
                var components = NSDateComponents()
                components.setValue(repeatEvery, forComponent: NSCalendarUnit.CalendarUnitMonth);
                duedate = NSCalendar.currentCalendar().dateByAddingComponents(components, toDate: duedate, options: NSCalendarOptions(0))!
            case (4):
                var components = NSDateComponents()
                components.setValue(repeatEvery, forComponent: NSCalendarUnit.CalendarUnitYear);
                duedate = NSCalendar.currentCalendar().dateByAddingComponents(components, toDate: duedate, options: NSCalendarOptions(0))!
            default:
                duedate = NSDate()
            }
            if (existingstartdate != nil) {
                let interval = existingstartdate!.timeIntervalSinceDate(existingduedate)
                startdate = duedate.dateByAddingTimeInterval(interval)
                newItem.setValue(startdate, forKey: "startdate")
            }
            
            // Override with new due date and id
            newItem.setValue(duedate, forKey: "duedate")
            var uuid = NSUUID().UUIDString
            newItem.setValue(uuid, forKey: "id")
            //println(newItem.valueForKey("name"))
            newItem.setValue(false, forKey: "completed")
            newItem.setValue(nil, forKey: "completeddate")
        }
        
        // Save data
        var error: NSError?
        if !managedContext.save(&error) {
            println("Could not save \(error), \(error?.userInfo)")
            let alert = UIAlertView()
            alert.title = "Error"
            alert.message = "Failed to save action"
            alert.addButtonWithTitle("Continue")
            alert.show()
        }
        
        /*
        if(detailViewController != nil && detailViewController.action == nil)
        {
        items.append(item)
        }
        else
        {*/
        // Updated view
        loadItems()
        //}
        
        self.reloadTableData()
        
        // todo: Check for other project / context changes
        if(item.valueForKey("projectid") != nil) {
            updateProjectTotals(item.valueForKey("projectid") as! String)
        }
        if(item.valueForKey("contextid") != nil) {
            updateContextTotals(item.valueForKey("contextid") as! String)
        }
        if(oldprojectid != nil && item.valueForKey("projectid") as? String != oldprojectid) {
            updateProjectTotals(oldprojectid!)
        }
        if(oldcontextid != nil && item.valueForKey("contextid") as? String != oldcontextid) {
            updateContextTotals(oldcontextid!)
        }
        
        updateTimeslotTotals()
    }
    
    func updateProjectTotals(projectid: String)
    {
        //1
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext!
        
        //2
        var fetchRequest = NSFetchRequest(entityName:"Action")
        
        var predicate = NSPredicate(format: "projectid == %@ AND trash == false", projectid)
        fetchRequest.predicate = predicate
        
        //3
        var error: NSError?
        
        var fetchedResults =
        managedContext.executeFetchRequest(fetchRequest,
            error: &error) as! [NSManagedObject]?
        
        var actions = [NSManagedObject]()
        
        if let results = fetchedResults {
            actions = results
        } else {
            println("Could not fetch \(error), \(error!.userInfo)")
        }
        
        var total : Int = 0
        var totalcost : Double = 0
        var totaltime : NSTimeInterval = 0
        var totalcostex : Double = 0
        var totaltimeex : NSTimeInterval = 0
        var totalex : Int = 0
        var totalcostclosed : Double = 0
        var totaltimeclosed : NSTimeInterval = 0
        var totalclosed : Int = 0
        
        for resultItem : AnyObject in actions {
            
            var time : NSTimeInterval = 0
            let cost = resultItem.valueForKey("cost") as! Double
            if resultItem.valueForKey("timeestimate") != nil {
                time = resultItem.valueForKey("timeestimate") as! NSTimeInterval
            }
            if(resultItem.valueForKey("completed") != nil && resultItem.valueForKey("completed") as! Bool)
            {
                totalclosed += 1
                totalcostclosed += cost
                totaltimeclosed += time
            }
            else
            {
                total += 1
                totalcost += cost
                totaltime += time
                if resultItem.valueForKey("priority") as! Int > 0 {
                    var startdate: NSDate? = resultItem.valueForKey("startdate") as? NSDate
                    if startdate != nil {
                        if NSDate().compare(startdate!) == NSComparisonResult.OrderedDescending {
                            totalcostex += cost
                            totalex += 1
                            totaltimeex += time
                        }
                    }
                    else {
                        totalcostex += cost
                        totalex += 1
                        totaltimeex += time
                    }
                }
            }
        }
        
        // Get project
        
        //1
        //2
        fetchRequest = NSFetchRequest(entityName:"Project")
        
        predicate = NSPredicate(format: "id == %@", projectid)
        
        // Set the predicate on the fetch request
        fetchRequest.predicate = predicate
        
        var items = [NSManagedObject]()
        
        fetchedResults =
            managedContext.executeFetchRequest(fetchRequest,
                error: &error) as! [NSManagedObject]?
        
        if let results = fetchedResults {
            items = results
        } else {
            println("Could not fetch \(error), \(error!.userInfo)")
        }
        if (items.count==1)
        {
            let item = items[0]
            
            item.setValue(total, forKey: "totalactions")
            item.setValue(totaltime, forKey: "totaltime")
            item.setValue(totalcost, forKey: "totalcosts")
            item.setValue(totalex, forKey: "totalactionsex")
            item.setValue(totalcostex, forKey: "totalcostsex")
            item.setValue(totaltimeex, forKey: "totaltimeex")
            item.setValue(totalclosed, forKey: "totalactionsclosed")
            item.setValue(totalcostclosed, forKey: "totalcostsclosed")
            item.setValue(totaltimeclosed, forKey: "totaltimeclosed")
            
            var error: NSError?
            if !managedContext.save(&error) {
                println("Could not save \(error), \(error?.userInfo)")
            }
        }
    }
    
    func updateContextTotals(contextid: String)
    {
        //1
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext!
        
        //2
        var fetchRequest = NSFetchRequest(entityName:"Action")
        
        var predicate = NSPredicate(format: "contextid == %@ AND (completed == false OR completed == nil) AND trash == false", contextid)
        fetchRequest.predicate = predicate
        
        //3
        var error: NSError?
        
        var fetchedResults =
        managedContext.executeFetchRequest(fetchRequest,
            error: &error) as! [NSManagedObject]?
        
        var actions = [NSManagedObject]()
        
        if let results = fetchedResults {
            actions = results
        } else {
            println("Could not fetch \(error), \(error!.userInfo)")
        }
        
        var totalcost : Double = 0
        var totaltime : NSTimeInterval = 0
        var totalcostex : Double = 0
        var totaltimeex : NSTimeInterval = 0
        var totalex : Int = 0
        var time: NSTimeInterval = 0
        
        for resultItem : AnyObject in actions {
            let cost = resultItem.valueForKey("cost") as! Double
            time = 0
            if resultItem.valueForKey("timeestimate") != nil {
                time = resultItem.valueForKey("timeestimate") as! NSTimeInterval
            }
            totalcost += cost
            if resultItem.valueForKey("priority") as! Int > 0 {
//                totalcostex += cost
//                totalex += 1
                var startdate: NSDate? = resultItem.valueForKey("startdate") as? NSDate
                if startdate != nil {
                    if NSDate().compare(startdate!) == NSComparisonResult.OrderedDescending {
                        totalcostex += cost
                        totalex += 1
                        totaltimeex += time
                    }
                }
                else {
                    totalcostex += cost
                    totalex += 1
                    totaltimeex += time
                }
            }
/*            if resultItem.valueForKey("timeestimate") != nil {
                let time = resultItem.valueForKey("timeestimate") as! NSTimeInterval
                totaltime += time
                if resultItem.valueForKey("priority") as! Int > 0 {
                    totaltimeex += time
                }*/
        }
        
        // Get context
        
        //1
        //2
        fetchRequest = NSFetchRequest(entityName:"Context")
        
        predicate = NSPredicate(format: "id == %@", contextid)
        
        // Set the predicate on the fetch request
        fetchRequest.predicate = predicate
        
        var items = [NSManagedObject]()
        
        fetchedResults =
            managedContext.executeFetchRequest(fetchRequest,
                error: &error) as! [NSManagedObject]?
        
        if let results = fetchedResults {
            items = results
        } else {
            println("Could not fetch \(error), \(error!.userInfo)")
        }
        if (items.count==1)
        {
            let item = items[0]
            
            item.setValue(totalcost, forKey: "totalcosts")
            item.setValue(totaltime, forKey: "totaltime")
            item.setValue(totalcostex, forKey: "totalcostsex")
            item.setValue(totaltimeex, forKey: "totaltimeex")
            item.setValue(actions.count, forKey: "totalactions")
            item.setValue(totalex, forKey: "totalactionsex")
            
            var error: NSError?
            if !managedContext.save(&error) {
                println("Could not save \(error), \(error?.userInfo)")
            }
        }
    }
    
    func updateTimeslotTotals()
    {
        //1
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext!
        
        //2
        var fetchRequest = NSFetchRequest(entityName:"Timeslot")
        
        //3
        var error: NSError?
        
        var fetchedResults =
        managedContext.executeFetchRequest(fetchRequest,
            error: &error) as! [NSManagedObject]?
        
        var timeslots = [NSManagedObject]()
        
        if let results = fetchedResults {
            timeslots = results
        } else {
            println("Could not fetch \(error), \(error!.userInfo)")
        }
        
        var totalcost : Double = 0
        var totaltime : NSTimeInterval = 0
        var totalcostex : Double = 0
        var totaltimeex : NSTimeInterval = 0
        var total: Int = 0
        var totalex : Int = 0
        var timeslotid : String
        
        for resultItem : AnyObject in timeslots {
            
            // Load projects
            
            var timeslotid = resultItem.valueForKey("id") as! String?
            if(timeslotid != nil)
            {
                var fetchRequest = NSFetchRequest(entityName:"Project")
                
                var predicate = NSPredicate(format: "timeslotid == %@", timeslotid!)
                
                // Set the predicate on the fetch request
                fetchRequest.predicate = predicate
                
                
                //3
                var error: NSError?
                
                var fetchedResults =
                managedContext.executeFetchRequest(fetchRequest,
                    error: &error) as! [NSManagedObject]?
                
                var projects = [NSManagedObject]()
                
                if let results = fetchedResults {
                    projects = results
                } else {
                    println("Could not fetch \(error), \(error!.userInfo)")
                }
                
                totalcost = 0
                totalcostex = 0
                totaltime = 0
                totaltimeex = 0
                total = 0
                totalex = 0
                
                for projectItem : AnyObject in projects {
                    if (projectItem.valueForKey("totalactions") != nil && projectItem.valueForKey("totalactionsex") != nil) {
                        let actions = projectItem.valueForKey("totalactions") as! Int
                        total += actions
                        let cost = projectItem.valueForKey("totalcosts") as! Double
                        totalcost += cost
                        let time = projectItem.valueForKey("totaltime") as! NSTimeInterval
                        totaltime += time
                        
                        let actionsex = projectItem.valueForKey("totalactionsex") as! Int
                        totalex += actionsex
                        let costex = projectItem.valueForKey("totalcostsex") as! Double
                        totalcostex += costex
                        let timeex = projectItem.valueForKey("totaltimeex") as! NSTimeInterval
                        totaltimeex += timeex
                    }
                    
                }
                
                // Update timeslot
                resultItem.setValue(totalcost, forKey: "totalcosts")
                resultItem.setValue(totaltime, forKey: "totaltime")
                resultItem.setValue(totalcostex, forKey: "totalcostsex")
                resultItem.setValue(totaltimeex, forKey: "totaltimeex")
                resultItem.setValue(total, forKey: "totalactions")
                resultItem.setValue(totalex, forKey: "totalactionsex")
                
                if !managedContext.save(&error) {
                    println("Could not save \(error), \(error?.userInfo)")
                }
            }
        }
    }
    
    override func canBecomeFirstResponder() -> Bool {
        return true
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        becomeFirstResponder()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        resignFirstResponder()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.resultSearchController = ({
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.hidesNavigationBarDuringPresentation = true
            controller.dimsBackgroundDuringPresentation = false
            //controller.searchBar.scopeButtonTitles = [ "All", "Completed"]
            //controller.searchBar.showsScopeBar = false
            controller.searchBar.sizeToFit()
            controller.searchBar.delegate = self
            
            //self.definesPresentationContext = true
            
            self.tableView.tableHeaderView = controller.searchBar
            
            return controller
        })()
        
        var toolBarItems = NSMutableArray()
        
        var systemButton1 = UIBarButtonItem(title: "Completed", style: .Plain, target: self, action: "toggleCompleted:")
        
        let font = UIFont.systemFontOfSize(11.0)
        systemButton1.setTitleTextAttributes([NSFontAttributeName: font], forState: UIControlState.Normal)

        systemButton1.tintColor = UIColor.blueColor()
        if(completed) {
            systemButton1.title = "Hide Completed"
        }
        else {
//            systemButton1.tintColor = UIColor.grayColor()
            systemButton1.title = "Show Completed"
        }
        toolBarItems.addObject(systemButton1)
        
        var systemButton2 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        toolBarItems.addObject(systemButton2)
        
        var systemButton3 = UIBarButtonItem(title: "Filter", style: .Plain, target: self, action: nil)
        
        statusButton.titleLabel?.numberOfLines = 2
        //        statusButton.setTitle("test\ntest", forState: UIControlState.Normal)
        statusButton.setTitleColor(UIColor.grayColor(), forState: UIControlState.Normal)
        statusButton.titleLabel!.font =  UIFont.systemFontOfSize(11.0)
        statusButton.titleLabel!.textAlignment = .Center
        statusButton.sizeToFit()
        //        button.targetForAction("actioncall", withSender: nil)
        systemButton3.customView = statusButton
        
        /*
        systemButton3.setTitleTextAttributes([NSFontAttributeName: font], forState: UIControlState.Normal)
        
        systemButton3.tintColor = UIColor.grayColor()
        systemButton3.title = "totals"*/
        toolBarItems.addObject(systemButton3)
        
        var systemButton4 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        toolBarItems.addObject(systemButton4)

        var systemButton5 = UIBarButtonItem(title: "Filter", style: .Plain, target: self, action: "toggleFilter:")
        
        systemButton5.setTitleTextAttributes([NSFontAttributeName: font], forState: UIControlState.Normal)
        
        systemButton5.tintColor = UIColor.blueColor()
        if(filter) {
            systemButton5.title = "Filter On"
        }
        else {
            systemButton3.title = "Filter Off"
        }
        toolBarItems.addObject(systemButton5)
       
        self.navigationController?.toolbarHidden = false
        self.setToolbarItems(toolBarItems as [AnyObject], animated: true)
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
   }

    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        loadItems()
        updateStatusButton()
    }
    
    func updateSearchResultsForSearchController(searchController: UISearchController)
    {
        filteredTableData.removeAll(keepCapacity: false)
        var searchText = searchController.searchBar.text
        var searchScope = searchController.searchBar.selectedScopeButtonIndex
        
        if !searchText.isEmpty {
            let searchPredicate = NSPredicate(format: "name CONTAINS[c] %@", searchText)
            
            let array = (items as NSArray).filteredArrayUsingPredicate(searchPredicate)
            filteredTableData = array as! [NSManagedObject]
            
            search = true
        }
        else {
            filteredTableData = items
            search = false
        }
        
        self.reloadTableData()
    }
    
    func searchBarShouldBeginEditing(searchBar: UISearchBar) -> Bool {
        //searchBar.setShowsCancelButton(false, animated: true)
        //searchBar.showsScopeBar = true
        searchBar.sizeToFit()
        return true
    }
    
    func searchBarShouldEndEditing(searchBar: UISearchBar) -> Bool {
        //searchBar.setShowsCancelButton(false, animated: true)
        //searchBar.showsScopeBar = false
        searchBar.sizeToFit()
        return true
    }
    
    func loadItems() {
        var actions = loadActionItems(mode, timeslotId, contextId, projectId, filter, completed)
        
        items = actions
        totalCost = 0.0
        totalTime = NSTimeInterval()
        
        for actionItem : AnyObject in actions {
            if (actionItem.valueForKey("cost") != nil ) {
                totalCost += actionItem.valueForKey("cost") as! Double
            }
            if (actionItem.valueForKey("timeestimate") != nil ) {
                totalTime += actionItem.valueForKey("timeestimate") as! NSTimeInterval
            }
        }
    }
    
/*
    func loadItemsOld() {
        //1
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext!
        
        //2
        let fetchRequest = NSFetchRequest(entityName:"Action")
        
        // Create a sort descriptor object that sorts on the "title"
        // property of the Core Data object
        let sortDescriptor0 = NSSortDescriptor(key: "completed", ascending: true)
        let sortDescriptor1 = NSSortDescriptor(key: "priority", ascending: false)
        let sortDescriptor2 = NSSortDescriptor(key: "creationdate", ascending: true)
        let sortDescriptor3 = NSSortDescriptor(key: "name", ascending: true, selector: "caseInsensitiveCompare:")
        
        // Set the list of sort descriptors in the fetch request,
        // so it includes the sort descriptor
        fetchRequest.sortDescriptors = [sortDescriptor0, sortDescriptor1, sortDescriptor2, sortDescriptor3]
        
        var subPredicates: [NSPredicate]! = []
        var subPredicate = NSPredicate()
        
        if mode == "inbox" {
            subPredicate = NSPredicate(format: "projectid == nil")
            subPredicates.append(subPredicate)
        }
        else if mode == "now" {
            
            // Select projects within this timeslot

            if !timeslotId.isEmpty {
                
                
                let fetchRequestProject = NSFetchRequest(entityName:"Project")
                let predicateProject = NSPredicate(format: "timeslotid == %@", timeslotId)
                
                fetchRequestProject.predicate = predicateProject
                
                var error: NSError?
                
                let fetchedResults =
                managedContext.executeFetchRequest(fetchRequestProject,
                    error: &error) as! [NSManagedObject]?
                
                var projects = [NSManagedObject]()
                
                if let results = fetchedResults {
                    projects = results
                } else {
                    println("Could not fetch \(error), \(error!.userInfo)")
                }
                
                var projectsA : [String] = [String]()
                
                for resultItem : AnyObject in projects {
                    let projectids = resultItem.valueForKey("id") as! String
                    projectsA.append(projectids)
                }
                
                // Filter on relevant projects
                
                subPredicate = NSPredicate(format: "projectid IN %@", projectsA)
                subPredicates.append(subPredicate)
            }
            
            // Exclude other location based contexts
            
            if !contextId.isEmpty {
                let fetchRequestContext = NSFetchRequest(entityName:"Context")
                let predicateContext = NSPredicate(format: "id != %@ and locationlat<>0", self.contextId)
                
                fetchRequestContext.predicate = predicateContext
                
                var error: NSError?
                
                let fetchedResults =
                managedContext.executeFetchRequest(fetchRequestContext,
                    error: &error) as! [NSManagedObject]?
                
                var contexts = [NSManagedObject]()
                
                if let results = fetchedResults {
                    contexts = results
                } else {
                    println("Could not fetch \(error), \(error!.userInfo)")
                }
                
                var contextsA : [String] = [String]()
                
                for resultItem : AnyObject in contexts {
                    let contextids = resultItem.valueForKey("id") as! String
                    contextsA.append(contextids)
                }
                
                subPredicate = NSPredicate(format: "NOT contextid IN %@", contextsA)
                subPredicates.append(subPredicate)
            }
        }
        else if project != nil {
            let projectid = project.valueForKey("id") as! String?
            
            subPredicate = NSPredicate(format: "projectid == %@", projectid!)
            subPredicates.append(subPredicate)
        }
        else if context != nil {
            let contextid = context.valueForKey("id") as! String?
            subPredicate = NSPredicate(format: "contextid == %@", contextid!)
            subPredicates.append(subPredicate)
        }
        else if timeslot != nil {
            
            // Select projects with this timeslot
            
            let fetchRequestProject = NSFetchRequest(entityName:"Project")
            let timeslotid = timeslot.valueForKey("id") as! String!
            let predicateProject = NSPredicate(format: "timeslotid == %@", timeslotid)
            
            fetchRequestProject.predicate = predicateProject
            
            var error: NSError?
            
            let fetchedResults =
            managedContext.executeFetchRequest(fetchRequestProject,
                error: &error) as! [NSManagedObject]?
            
            var projects = [NSManagedObject]()
            
            if let results = fetchedResults {
                projects = results
            } else {
                println("Could not fetch \(error), \(error!.userInfo)")
            }
            
            var projectsA : [String] = [String]()
            
            for resultItem : AnyObject in projects {
                let projectids = resultItem.valueForKey("id") as! String
                projectsA.append(projectids)
            }
            
            // Filter on relevant projects
            
            subPredicate = NSPredicate(format: "projectid IN %@", projectsA)
            subPredicates.append(subPredicate)
        }
        
        if (filter) {
            subPredicate = NSPredicate(format: "priority > 0 AND (startdate <= %@ OR startdate == nil)", NSDate())
            subPredicates.append(subPredicate)
        }
        
        
        if (completed) {
            subPredicate = NSPredicate(format: "trash == false")
        }
        else {
            subPredicate = NSPredicate(format: "completed = false AND trash == false")
        }
        subPredicates.append(subPredicate)
        
        // Set the predicate on the fetch request
        //fetchRequest.predicate = predicate
        fetchRequest.predicate = NSCompoundPredicate.andPredicateWithSubpredicates(subPredicates)
        
        
        //3
        var error: NSError?
        
        let fetchedResults =
        managedContext.executeFetchRequest(fetchRequest,
            error: &error) as! [NSManagedObject]?
        
        if let results = fetchedResults {
            items = results
        } else {
            println("Could not fetch \(error), \(error!.userInfo)")
        }
    }
*/
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    func reloadTableData () {
        self.tableView.reloadData()
        updateStatusButton()
    }
    
    func updateStatusButton () {
        var timetext : String? = ""
        //        if totalTime != nil{
        let dcf = NSDateComponentsFormatter()
        dcf.zeroFormattingBehavior = .Pad
        dcf.allowedUnits = .CalendarUnitHour | .CalendarUnitMinute
        timetext = dcf.stringFromTimeInterval(totalTime)
        //        }
        
        var formatter = NSNumberFormatter()
        formatter.numberStyle = .CurrencyStyle
        
        statusButton.setTitle(String(format: "Time: %@\nCost: %@",timetext!,formatter.stringFromNumber(totalCost)!), forState: UIControlState.Normal)
        statusButton.sizeToFit()
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        if (self.resultSearchController.active) {
            return self.filteredTableData.count
        }
        else {
            return items.count
        }
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ActionCell", forIndexPath: indexPath) as! UITableViewCell
        
        var item:NSManagedObject!
        // Configure the cell...
        if (self.resultSearchController.active) {
            item = filteredTableData[indexPath.row]
        }
        else {
            item = items[indexPath.row]
        }
        cell.textLabel!.text = item.valueForKey("name") as! String?
        
        var priority : Int!
        
        priority = item.valueForKey("priority") as! Int?
        
        let cost : Float? = item.valueForKey("cost") as! Float?
        
        var priorityText : String?
        
        switch (priority) {
        case (1):
            priorityText = "!"
        case (2):
            priorityText = "!!"
        case (3):
            priorityText = "!!!"
        default:
            priorityText = "="
        }
        if (item.valueForKey("completed") != nil) {
            if (item.valueForKey("completed") as! Bool == true) {
                priorityText = "Done"
            }
        }
        
        var time: NSTimeInterval! = item.valueForKey("timeestimate") as! NSTimeInterval?
        var timetext : String? = ""
        if time != nil{
            let dcf = NSDateComponentsFormatter()
            dcf.zeroFormattingBehavior = .Pad
            dcf.allowedUnits = .CalendarUnitHour | .CalendarUnitMinute
            timetext = dcf.stringFromTimeInterval(time)
        }
        
        var formatter = NSNumberFormatter()
        formatter.numberStyle = .CurrencyStyle
        
        cell.detailTextLabel!.text = NSString(format: "%@ | %@ | %@", priorityText!, timetext!, formatter.stringFromNumber(cost!)!) as String
        
        return cell
    }
    
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    
    
    override func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [AnyObject]? {
        
        let deleteClosure = { (action: UITableViewRowAction!, indexPath: NSIndexPath!) -> Void in
            
            self.deleteItem(self.items[indexPath.row])
            
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        }
        
        let deleteAction = UITableViewRowAction(style: .Default, title: "Delete", handler: deleteClosure)
        
        let doneClosure = { (action: UITableViewRowAction!, indexPath: NSIndexPath!) -> Void in
            
            /*            let appDelegate =
            UIApplication.sharedApplication().delegate as! AppDelegate
            
            var managedContext : NSManagedObjectContext = appDelegate.managedObjectContext!*/
            var item : NSManagedObject = self.items[indexPath.row]
            
            self.saveAction(item, completed: true, oldprojectid: nil, oldcontextid: nil)
        }
        
        let doneAction = UITableViewRowAction(style: .Normal, title: "Done", handler: doneClosure)
        return [deleteAction, doneAction]
    }
    
    func deleteItem(itemToDelete:NSManagedObject)
    {
        undoManager.registerUndoWithTarget(self, selector:Selector("deleteItem:"), object:itemToDelete)
        undoManager.setActionName("delete action")
        
        let projectid : String? = itemToDelete.valueForKey("projectid") as! String?
        let contextid : String? = itemToDelete.valueForKey("contextid") as! String?
        
        // Delete it from the managedObjectContext
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext!
        //managedContext.deleteObject(itemToDelete)
        
        if self.undoer.undoing {
            itemToDelete.setValue(false, forKey: "trash")
        }
        else
        {
            itemToDelete.setValue(true, forKey: "trash")
        }
        
        var error: NSError?
        if !managedContext.save(&error) {
            println("Could not save \(error), \(error?.userInfo)")
        }
        
        // Update totals
        if(projectid != nil) {
            self.updateProjectTotals(projectid!)
        }
        if(contextid != nil) {
            self.updateContextTotals(contextid!)
        }
        self.updateTimeslotTotals()
        
        // Refresh the table view to indicate that it's deleted
        self.loadItems()
        
        if self.undoer.undoing || self.undoer.redoing {
            reloadTableData()
        }
    }
    
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            //            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
    
    }
    */
    
    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the item to be re-orderable.
    return true
    }
    */
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        if segue.identifier == "ShowActionDetail" {
            var indexPath:NSIndexPath = self.tableView.indexPathForSelectedRow()!
            var item: NSManagedObject
            if search {
                item = filteredTableData[indexPath.row]
            } else {
               item = items[indexPath.row]
            }
            let vc = segue.destinationViewController as! ActionDetailViewController
            vc.action = item
            vc.title = "Action"
            vc.delegate = self
        }
        if segue.identifier == "ShowNewAction" {
            let vc = segue.destinationViewController as! ActionDetailViewController
            if !projectId.isEmpty {
                vc.projectid = projectId
            }
            if !contextId.isEmpty {
                vc.contextid = contextId
            }
            /*
            if project != nil {
                vc.projectid = project.valueForKey("id") as! String!
            }
            if context != nil {
                vc.contextid = context.valueForKey("id") as! String!
            }*/
            
        }

        resultSearchController.active = false
    }
}
