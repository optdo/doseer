//
//  ProjectsViewController.swift
//  next3
//
//  Created by Bastiaan on 25/02/2015.
//  Copyright (c) 2015 Bastiaan. All rights reserved.
//

import UIKit
import CoreData

class ProjectsViewController: UITableViewController, ProjectDetailDelegate {
    
    let undoer = NSUndoManager()
    override var undoManager : NSUndoManager {
        get {
            return self.undoer
        }
    }
    
    @IBOutlet var filterBarButton: UIBarButtonItem!
    
    @IBOutlet var completedBarButton: UIBarButtonItem!
    
    var filter: Bool = true
    var completed: Bool = false
    var items = [NSManagedObject]()

    @IBAction func cancelToProjectsViewController(segue:UIStoryboardSegue) {
    }
    
    func toggleFilter(sender: UIBarButtonItem)
    {
        if(filter) {
            filter = false
            sender.title = "Filter Off"
        }
        else {
            filter = true
            sender.title = "Filter On"
        }
        self.tableView.reloadData()
    }
    
    func toggleCompleted(sender: UIBarButtonItem)
    {
        if(completed) {
            completed = false
            sender.title = "Show Completed"
        }
        else {
            completed = true
            sender.title = "Hide Completed"
        }
        loadItems()
        self.tableView.reloadData()
    }
    
    
    @IBAction func saveProjectDetail(segue:UIStoryboardSegue) {
        
        
            let detailViewController = segue.sourceViewController as! ProjectDetailViewController
            
            //1
            let appDelegate =
            UIApplication.sharedApplication().delegate as! AppDelegate
            
            let managedContext = appDelegate.managedObjectContext!
            
            if(detailViewController.project == nil)
            {
                //2
                let entity =  NSEntityDescription.entityForName("Project",
                    inManagedObjectContext:
                    managedContext)
                
                let item = NSManagedObject(entity: entity!,
                    insertIntoManagedObjectContext:managedContext)
                
                //3
                item.setValue(detailViewController.nameTextField.text, forKey: "name")
                item.setValue(detailViewController.timeslotid, forKey: "timeslotid")
                /*if detailViewController.dueDateLabel != "Not set" {
                    item.setValue(detailViewController.dueDatePicker.date, forKey: "duedate")
                }*/
                if (detailViewController.sharedSegmentedControl.selectedSegmentIndex == 0)
                {
                    item.setValue(true, forKey: "shared")
                }
                else
                {
                    item.setValue(false, forKey: "shared")
                }
                if (detailViewController.completedSegmentedControl.selectedSegmentIndex == 0)
                {
                    item.setValue(true, forKey: "completed")
                }
                else
                {
                    item.setValue(false, forKey: "completed")
                }
                var uuid = NSUUID().UUIDString
                item.setValue(uuid, forKey: "id")
                item.setValue(false, forKey: "trash")

                //4
                var error: NSError?
                if !managedContext.save(&error) {
                    println("Could not save \(error), \(error?.userInfo)")
                }
                //5
                items.append(item)
            }
            else
            {
                let item = detailViewController.project;
                
                item.setValue(detailViewController.nameTextField.text, forKey: "name")
                item.setValue(detailViewController.timeslotid, forKey: "timeslotid")
                /*item.setValue(detailViewController.dueDatePicker.date, forKey: "duedate")
                if detailViewController.dueDateLabel != "Not set" {
                    item.setValue(detailViewController.dueDatePicker.date, forKey: "duedate")
                }*/
                if (detailViewController.sharedSegmentedControl.selectedSegmentIndex == 0)
                {
                    item.setValue(true, forKey: "shared")
                }
                else
                {
                    item.setValue(false, forKey: "shared")
                }
                if (detailViewController.completedSegmentedControl.selectedSegmentIndex == 0)
                {
                    item.setValue(true, forKey: "completed")
                }
                else
                {
                    item.setValue(false, forKey: "completed")
                }
                
                var error: NSError?
                if !managedContext.save(&error) {
                    println("Could not save \(error), \(error?.userInfo)")
                }
                
                // Updated view
                //loadItems()
            }
        //self.tableView.reloadData()
    }
    
    func deleteProject(controller: ProjectDetailViewController) {
        
        deleteItem(controller.project)
        
        self.tableView.reloadData()
        
        controller.navigationController?.popViewControllerAnimated(true)
    }
    
    func deleteItem(itemToDelete:NSManagedObject)
    {
        undoManager.registerUndoWithTarget(self, selector:Selector("deleteItem:"), object:itemToDelete)
        undoManager.setActionName("delete project")
        
        // Delete it from the managedObjectContext
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext!
        
        if self.undoer.undoing {
            itemToDelete.setValue(false, forKey: "trash")
        }
        else
        {
            itemToDelete.setValue(true, forKey: "trash")
        }
        
        var error: NSError?
        if !managedContext.save(&error) {
            println("Could not save \(error), \(error?.userInfo)")
        }
        // TODO: Update all actions to remove project id
        
        // Refresh the table view to indicate that it's deleted
        self.loadItems()
        
        if self.undoer.undoing || self.undoer.redoing {
            self.tableView.reloadData()
        }
    }
    
    override func canBecomeFirstResponder() -> Bool {
        return true
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        becomeFirstResponder()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        resignFirstResponder()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.filterBarButton.action = Selector("toggleFilter:")
        self.completedBarButton.action = Selector("toggleCompleted:")

        let font = UIFont.systemFontOfSize(11.0)
        font
        filterBarButton.setTitleTextAttributes([NSFontAttributeName: font, NSForegroundColorAttributeName: UIColor.blueColor()],forState: UIControlState.Normal)
        completedBarButton.setTitleTextAttributes([NSFontAttributeName: font, NSForegroundColorAttributeName: UIColor.blueColor()],forState: UIControlState.Normal)

        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        loadItems()
        self.tableView.reloadData()
    }
    
    func loadItems() {
        //1
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext!
        
        //2
        let fetchRequest = NSFetchRequest(entityName:"Project")
        
        let sortDescriptor1 = NSSortDescriptor(key: "name", ascending: true, selector: "caseInsensitiveCompare:")
        fetchRequest.sortDescriptors = [sortDescriptor1]

        var predicate = NSPredicate()
        
        if (completed) {
            predicate = NSPredicate(format: "(trash == false OR trash == nil)")
        }
        else {
            predicate = NSPredicate(format: "(completed == false OR completed == nil) AND (trash == false OR trash == nil)")
        }
        
        // Set the predicate on the fetch request
        fetchRequest.predicate = predicate

        //3
        var error: NSError?
        
        let fetchedResults =
        managedContext.executeFetchRequest(fetchRequest,
            error: &error) as! [NSManagedObject]?
        
        if let results = fetchedResults {
            items = results
        } else {
            println("Could not fetch \(error), \(error!.userInfo)")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        return items.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ProjectCell", forIndexPath: indexPath) as! UITableViewCell

        // Configure the cell...
        let item = items[indexPath.row]
        cell.textLabel!.text = item.valueForKey("name") as! String?
        
        if ( item.valueForKey("totalactions") != nil && item.valueForKey("totalactionsex") != nil)
        {
            
            let totalactions = item.valueForKey("totalactions") as! Double
            let totalcost = item.valueForKey("totalcosts") as! Double
            let totaltime = item.valueForKey("totaltime") as! NSTimeInterval?
            
            let totalactionsex = item.valueForKey("totalactionsex") as! Double
            let totalcostex = item.valueForKey("totalcostsex") as! Double
            let totaltimeex = item.valueForKey("totaltimeex") as! NSTimeInterval?
            
            //       var time: NSTimeInterval! = item.valueForKey("timeestimate") as! NSTimeInterval?
            var timetext : String? = ""
            if totaltime != nil{
                let dcf = NSDateComponentsFormatter()
                dcf.zeroFormattingBehavior = .Pad
                dcf.allowedUnits = .CalendarUnitHour | .CalendarUnitMinute
                timetext = dcf.stringFromTimeInterval(totaltime!)
            }
            var timetextex : String? = ""
            if totaltimeex != nil{
                let dcf = NSDateComponentsFormatter()
                dcf.zeroFormattingBehavior = .Pad
                dcf.allowedUnits = .CalendarUnitHour | .CalendarUnitMinute
                timetextex = dcf.stringFromTimeInterval(totaltimeex!)
            }
            
            if totalactions > 0
            {
                var formatter = NSNumberFormatter()
                formatter.numberStyle = .CurrencyStyle
                
                if filter {
                    cell.detailTextLabel!.text = NSString(format: "%.0f actions | %@ | %@", totalactionsex, timetextex!, formatter.stringFromNumber(totalcostex)!) as String
                }
                else {
                    cell.detailTextLabel!.text = NSString(format: "%.0f actions | %@ | %@", totalactions, timetext!, formatter.stringFromNumber(totalcost)!) as String
                }
            }
            else {
                cell.detailTextLabel!.text = "No actions"
            }
        }
        else {
            cell.detailTextLabel!.text = "Not updated"
        }

        return cell
    }
    

    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }

    override func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [AnyObject]? {
        
        let deleteClosure = { (action: UITableViewRowAction!, indexPath: NSIndexPath!) -> Void in
            let itemToDelete = self.items[indexPath.row]
            
            self.deleteItem(itemToDelete)
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        }
        
        let editClosure = { (action: UITableViewRowAction!, indexPath: NSIndexPath!) -> Void in
            self.performSegueWithIdentifier("ShowProjectDetail",sender:indexPath)
        }
        let summaryClosure = { (action: UITableViewRowAction!, indexPath: NSIndexPath!) -> Void in
            self.performSegueWithIdentifier("ShowProjectSummary",sender:indexPath)
        }
        
        let deleteAction = UITableViewRowAction(style: .Default, title: "Delete", handler: deleteClosure)
        let editAction = UITableViewRowAction(style: .Normal, title: "Edit", handler: editClosure)
        let summaryAction = UITableViewRowAction(style: .Normal, title: "Summary", handler: summaryClosure)
        
        summaryAction.backgroundColor = UIColor.darkGrayColor()
        
        return [deleteAction, editAction, summaryAction]
    }
    

    // Override to support editing the table view.
    
    override func tableView(tableView: (UITableView!), commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
            
        if editingStyle == .Delete {

        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        if segue.identifier == "ShowProjectDetail" {
            var indexPath = sender as! (NSIndexPath)
            var item = items[indexPath.row]
            let vc = segue.destinationViewController as! ProjectDetailViewController
            vc.project = item
            vc.title = "Project"
            vc.delegate = self
        }
        if segue.identifier == "ShowProjectSummary" {
            var indexPath = sender as! (NSIndexPath)
            var item = items[indexPath.row]
            let vc = segue.destinationViewController as! ProjectSummaryViewController
            vc.project = item
        }
        if segue.identifier == "ShowActionsFromProjects" {
            var indexPath: NSIndexPath! = self.tableView.indexPathForSelectedRow()
            var item = items[indexPath.row]
            let vc = segue.destinationViewController as! ActionsViewController
//            vc.project = item
            vc.projectId = item.valueForKey("id") as! String

        }
    }
    

}
