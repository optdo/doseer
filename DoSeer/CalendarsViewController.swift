//
//  CalendarsViewController.swift
//  next3
//
//  Created by Bastiaan on 25/02/2015.
//  Copyright (c) 2015 Bastiaan. All rights reserved.
//

import UIKit
import CoreData

class CalendarsViewController: UITableViewController, CalendarDetailDelegate {

    @IBAction func cancelToCalendarsViewController(segue:UIStoryboardSegue) {
    }
    
    @IBAction func saveCalendarDetail(segue:UIStoryboardSegue) {

        let detailViewController = segue.sourceViewController as! CalendarDetailViewController
        
        //1
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext!
        
        if(detailViewController.calendar == nil)
        {
            //2
            let entity =  NSEntityDescription.entityForName("Calendar",
                inManagedObjectContext:
                managedContext)
            
            let item = NSManagedObject(entity: entity!,
                insertIntoManagedObjectContext:managedContext)
            
            //3
            item.setValue(detailViewController.nameTextField.text, forKey: "name")
            item.setValue(detailViewController.typeLabel.text, forKey: "type")
            item.setValue(detailViewController.calendarid, forKey: "externalid")
            
            var uuid = NSUUID().UUIDString
            item.setValue(uuid, forKey: "id")
            item.setValue(false, forKey: "trash")
            
            //4
            var error: NSError?
            if !managedContext.save(&error) {
                println("Could not save \(error), \(error?.userInfo)")
            }
            //5
            items.append(item)
        }
        else
        {
            let item = detailViewController.calendar;
            
            item.setValue(detailViewController.nameTextField.text, forKey: "name")
            item.setValue(detailViewController.typeLabel.text, forKey: "type")
            item.setValue(detailViewController.calendarid, forKey: "externalid")
            
            var error: NSError?
            if !managedContext.save(&error) {
                println("Could not save \(error), \(error?.userInfo)")
            }
            
            // Updated view
            //loadItems()
        }
        //self.tableView.reloadData()
    }
    
    let undoer = NSUndoManager()
    override var undoManager : NSUndoManager {
        get {
            return self.undoer
        }
    }
    
    var items = [NSManagedObject]()
    
    
    func deleteCalendar(controller: CalendarDetailViewController) {
        
        deleteItem(controller.calendar)
        
        self.tableView.reloadData()
        
        controller.navigationController?.popViewControllerAnimated(true)
    }
    
    func deleteItem(itemToDelete:NSManagedObject)
    {
        undoManager.registerUndoWithTarget(self, selector:Selector("deleteItem:"), object:itemToDelete)
        undoManager.setActionName("delete calendar")
        
        // Delete it from the managedObjectContext
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext!
        
        if self.undoer.undoing {
            itemToDelete.setValue(false, forKey: "trash")
        }
        else
        {
            itemToDelete.setValue(true, forKey: "trash")
        }
        
        var error: NSError?
        if !managedContext.save(&error) {
            println("Could not save \(error), \(error?.userInfo)")
        }
        // TODO: Update all actions to remove project id
        
        // Refresh the table view to indicate that it's deleted
        self.loadItems()
        
        if self.undoer.undoing || self.undoer.redoing {
            self.tableView.reloadData()
        }
    }
    
    override func canBecomeFirstResponder() -> Bool {
        return true
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        becomeFirstResponder()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        resignFirstResponder()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        loadItems()
        self.tableView.reloadData()
    }
    
    func loadItems() {
        //1
        let appDelegate =
        UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext!
        
        //2
        let fetchRequest = NSFetchRequest(entityName:"Calendar")
        
        let sortDescriptor1 = NSSortDescriptor(key: "name", ascending: true, selector: "caseInsensitiveCompare:")
        fetchRequest.sortDescriptors = [sortDescriptor1]
        
        let predicate = NSPredicate(format: "(trash == false OR trash == nil)")
        
        // Set the predicate on the fetch request
        fetchRequest.predicate = predicate
        
        //3
        var error: NSError?
        
        let fetchedResults =
        managedContext.executeFetchRequest(fetchRequest,
            error: &error) as! [NSManagedObject]?
        
        if let results = fetchedResults {
            items = results
        } else {
            println("Could not fetch \(error), \(error!.userInfo)")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        return items.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CalendarCell", forIndexPath: indexPath) as! UITableViewCell
        
        // Configure the cell...
        let item = items[indexPath.row]
        cell.textLabel!.text = item.valueForKey("name") as! String?
        
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        if segue.identifier == "ShowCalendarDetail" {
            var indexPath: NSIndexPath! = self.tableView.indexPathForSelectedRow()
            var item = items[indexPath.row]
            let vc = segue.destinationViewController as! CalendarDetailViewController
            vc.calendar = item
            vc.title = "Calendar"
            vc.delegate = self
        }
    }
}
