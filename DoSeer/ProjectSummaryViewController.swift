//
//  ProjectSummaryViewController.swift
//  next3.1.2
//
//  Created by Bastiaan on 17/04/2015.
//  Copyright (c) 2015 Bastiaan. All rights reserved.
//

import UIKit
import CoreData

class ProjectSummaryViewController: UITableViewController {

    var project : NSManagedObject!
    
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var openActionsLabel: UILabel!
    @IBOutlet var openActionsExLabel: UILabel!
    @IBOutlet var openTimeLabel: UILabel!
    @IBOutlet var openTimeExLabel: UILabel!
    @IBOutlet var openCostsLabel: UILabel!
    @IBOutlet var openCostsExLabel: UILabel!
    @IBOutlet var closedActionsLabel: UILabel!
    @IBOutlet var closedTimeHours: UILabel!
    @IBOutlet var closedCostsLabel: UILabel!
    @IBOutlet var closedTimeLabel: UILabel!
    @IBOutlet var ETALabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        if (project != nil)
        {
            self.nameLabel.text = project.valueForKey("name") as! String?
            
            let totalactions = project.valueForKey("totalactions") as! Double
            let totalcost = project.valueForKey("totalcosts") as! Double
            let totaltime = project.valueForKey("totaltime") as! NSTimeInterval?
            
            let totalactionsex = project.valueForKey("totalactionsex") as! Double
            let totalcostex = project.valueForKey("totalcostsex") as! Double
            let totaltimeex = project.valueForKey("totaltimeex") as! NSTimeInterval?
            
            let totalactionsclosed = project.valueForKey("totalactionsclosed") as! Double
            let totalcostclosed = project.valueForKey("totalcostsclosed") as! Double
            let totaltimeclosed = project.valueForKey("totaltimeclosed") as! NSTimeInterval?
 
            //       var time: NSTimeInterval! = item.valueForKey("timeestimate") as! NSTimeInterval?
            var timetext : String? = ""
            if totaltime != nil{
                let dcf = NSDateComponentsFormatter()
                dcf.zeroFormattingBehavior = .Pad
                dcf.allowedUnits = .CalendarUnitHour | .CalendarUnitMinute
                timetext = dcf.stringFromTimeInterval(totaltime!)
            }
            var timetextex : String? = ""
            if totaltimeex != nil{
                let dcf = NSDateComponentsFormatter()
                dcf.zeroFormattingBehavior = .Pad
                dcf.allowedUnits = .CalendarUnitHour | .CalendarUnitMinute
                timetextex = dcf.stringFromTimeInterval(totaltimeex!)
            }
            var timetextclosed : String? = ""
            if totaltimeclosed != nil{
                let dcf = NSDateComponentsFormatter()
                dcf.zeroFormattingBehavior = .Pad
                dcf.allowedUnits = .CalendarUnitHour | .CalendarUnitMinute
                timetextclosed = dcf.stringFromTimeInterval(totaltimeclosed!)
            }
            
            openActionsLabel.text = NSString(format: "%.0f", totalactions) as String
            openActionsExLabel.text = NSString(format: "%.0f", totalactionsex) as String
            openTimeLabel.text = timetext
            openTimeExLabel.text = timetextex
            openCostsLabel.text = NSString(format: "%.2f", totalcost) as String
            openCostsExLabel.text = NSString(format: "%.2f", totalcostex) as String

            closedActionsLabel.text = NSString(format: "%.0f", totalactionsclosed) as String
            closedTimeLabel.text = timetextclosed
            closedCostsLabel.text = NSString(format: "%.2f", totalcostclosed) as String
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 4
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        if (section==0)
        {
            return 1
        }
        if (section==1)
        {
            return 3
        }
        if (section==2)
        {
            return 3
        }
        if (section==3)
        {
            return 3
        }
        return 0
    }

    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as! UITableViewCell

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        if segue.identifier == "ShowActionsFromProjectSummary" {
            let vc = segue.destinationViewController as! ActionsViewController
//            vc.project = project
            vc.projectId = project.valueForKey("id") as! String
            vc.completed = true
        }
    }
    

}
