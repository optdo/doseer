//
//  ContextLocationViewController.swift
//  DoSeer
//
//  Created by Bastiaan on 28/07/2015.
//  Copyright (c) 2015 Bastiaan. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class ContextLocationViewController: UIViewController, CLLocationManagerDelegate, UISearchResultsUpdating, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UISearchControllerDelegate {
    
    var searchController: UISearchController?
    
    @IBOutlet var mapView: MKMapView!
    @IBOutlet var resultTable: UITableView!
    
    var mapItems : [MKMapItem] = [MKMapItem]()
    
    var selectedLocation: CLLocation!
    var selectedLocationInfo: String = ""
    
    var locationManager: CLLocationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        if (selectedLocation != nil) {
            var placeMark = MKPlacemark(coordinate: selectedLocation.coordinate, addressDictionary: nil)
            self.updateMap(placeMark, title: selectedLocationInfo)
        }
        
        searchController = ({
            let searchController = UISearchController(searchResultsController: nil)
            searchController.searchResultsUpdater = self
            searchController.hidesNavigationBarDuringPresentation = false
            searchController.dimsBackgroundDuringPresentation = false
            
            //setup the search bar
            searchController.searchBar.showsCancelButton = false
            
            searchController.searchBar.autoresizingMask = UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing.FlexibleHeight
            self.resultTable.tableHeaderView = searchController.searchBar
            searchController.searchBar.sizeToFit()
            searchController.searchBar.delegate = self
            searchController.delegate = self
            
            return searchController
        })()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.searchController!.active = false
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        var currentLoc : MKMapItem = MKMapItem()
        currentLoc.name = "Current Location"
        self.mapItems.append(currentLoc)
        self.resultTable.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //    func searchBarShouldBeginEditing(searchBar: UISearchBar) -> Bool {
    //        searchBar.setShowsCancelButton(false, animated: true)
    //        searchBar.showsCancelButton = false
    //        searchBar.sizeToFit()
    //        return true
    //    }
    
    
    func searchBarShouldEndEditing(searchBar: UISearchBar) -> Bool {
        //searchBar.setShowsCancelButton(false, animated: true)
        //searchBar.showsScopeBar = false
        //searchBar.sizeToFit()
        searchBar.resignFirstResponder()
        return true
    }
    
    //MARK:UISearchResultsUpdating
    
    func didPresentSearchController(searchController: UISearchController) {
        searchController.searchBar.showsCancelButton = false
    }
    
    func updateSearchResultsForSearchController(searchController: UISearchController)
    {
        self.lookupLocation()
        
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!)
    {
        var latestLocation = locations[locations.count - 1] as! CLLocation
        
        selectedLocation = latestLocation
        
        // Add below code to get address for touch coordinates.
        
        let geoCoder = CLGeocoder()
        
        geoCoder.reverseGeocodeLocation(latestLocation, completionHandler: { (placemarks, error) -> Void in
            let placeArray = placemarks as? [CLPlacemark]
            
            // Place details
            var placeMark: CLPlacemark!
            placeMark = placeArray?[0]
            
            if (placeMark != nil) {
                // Address dictionary
                println(placeMark.addressDictionary)
                
                // Location name
                if let locationName = placeMark.addressDictionary["Name"] as? NSString {
                    println(locationName)
                }
                
                // Street address
                if let street = placeMark.addressDictionary["Thoroughfare"] as? NSString {
                    println(street)
                }
                
                // City
                if let city = placeMark.addressDictionary["City"] as? NSString {
                    println(city)
                }
                
                // Zip code
                if let zip = placeMark.addressDictionary["ZIP"] as? NSString {
                    println(zip)
                }
                
                // Country
                if let country = placeMark.addressDictionary["Country"] as? NSString {
                    println(country)
                }
                self.selectedLocationInfo = placeMark.addressDictionary["Name"] as! String
                
                self.updateMap(MKPlacemark(placemark: placeMark), title: (placeMark.addressDictionary["Name"] as? String)!)
            }
        })
        
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(manager: CLLocationManager!,
        didFailWithError error: NSError!) {
            
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mapItems.count
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("LocationSelectorCell") as! UITableViewCell
        
        if indexPath.row == 0 {
            cell.textLabel!.text = "Current Location"
            cell.detailTextLabel!.text = ""
        }
        else {
            
            cell.textLabel!.text = mapItems[indexPath.row].name
            var locality = ""
            if (mapItems[indexPath.row].placemark.locality != nil) {
                locality = mapItems[indexPath.row].placemark.locality
            }
            var country = ""
            if (mapItems[indexPath.row].placemark.country != nil) {
                country = mapItems[indexPath.row].placemark.country
            }
            cell.detailTextLabel!.text  = String(format: "%@ %@",  locality, country)
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        searchController!.searchBar.resignFirstResponder()
        
        if indexPath.row == 0 {
            locationManager.startUpdatingLocation()
        }
        else {
            
            let curLoc = mapItems[indexPath.row].placemark.coordinate
            var curCLloc: CLLocation =  CLLocation(latitude: curLoc.latitude, longitude: curLoc.longitude)
            
            selectedLocation  = curCLloc
            selectedLocationInfo = mapItems[indexPath.row].name
            
            updateMap(mapItems[indexPath.row].placemark, title: mapItems[indexPath.row].name)
        }
        
    }
    
    func updateMap (placemark: MKPlacemark, title: String) {
        
        mapView.hidden = false
        
        if self.mapView.annotations.count != 0 {
            let annotation = self.mapView.annotations[0] as! MKAnnotation
            self.mapView.removeAnnotation(annotation)
        }
        
        var pointAnnotation:MKPointAnnotation!
        var pinAnnotationView:MKPinAnnotationView!
        
        pointAnnotation = MKPointAnnotation()
        pointAnnotation.title = title
        pointAnnotation.coordinate = placemark.coordinate
        
        pinAnnotationView = MKPinAnnotationView(annotation: pointAnnotation, reuseIdentifier: nil)
        self.mapView.centerCoordinate = pointAnnotation.coordinate
        self.mapView.addAnnotation(pinAnnotationView.annotation)
        
        var region = MKCoordinateRegionMakeWithDistance(placemark.coordinate, 10, 10)
        self.mapView.setRegion(region, animated: true)
    }
    
    func lookupLocation() {
        var annotation:MKAnnotation!
        var localSearchRequest:MKLocalSearchRequest!
        var localSearch:MKLocalSearch!
        var localSearchResponse:MKLocalSearchResponse!
        var error:NSError!
        
        localSearchRequest = MKLocalSearchRequest()
        localSearchRequest.naturalLanguageQuery = self.searchController!.searchBar.text
        localSearch = MKLocalSearch(request: localSearchRequest)
        localSearch.startWithCompletionHandler { (localSearchResponse, error) -> Void in
            
            if localSearchResponse == nil{
                return
            }
            self.mapItems = [MKMapItem]()
            
            var currentLoc : MKMapItem = MKMapItem()
            currentLoc.name = "Current Location"
            self.mapItems.append(currentLoc)
            
            for item in localSearchResponse.mapItems as! [MKMapItem] {
                self.mapItems.append(item)
            }
            
            self.resultTable.reloadData()
        }
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
