//
//  CalendarSelectorViewController.swift
//  DoSeer
//
//  Created by Bastiaan on 28/07/2015.
//  Copyright (c) 2015 Bastiaan. All rights reserved.
//


import UIKit
import EventKit

class CalendarSelectorViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let eventStore = EKEventStore()
    
    var selectedItem:String? = nil
    var selectedItemId:String? = nil
    var selectedItemIndex:Int? = nil

    @IBOutlet weak var needPermissionView: UIView!
    @IBOutlet weak var calendarTableView: UITableView!
        
    var calendars: [EKCalendar]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    
    override func viewWillAppear(animated: Bool) {
        checkCalendarAuthorizationStatus()
    }
    
    func checkCalendarAuthorizationStatus() {
        let status = EKEventStore.authorizationStatusForEntityType(EKEntityTypeEvent)
        
        switch (status) {
        case EKAuthorizationStatus.NotDetermined:
            // This happens on first-run
            requestAccessToCalendar()
        case EKAuthorizationStatus.Authorized:
            // Things are in line with being able to show the calendars in the table view
            loadCalendars()
            refreshTableView()
        case EKAuthorizationStatus.Restricted, EKAuthorizationStatus.Denied:
            // We need to help them give us permission
            needPermissionView.fadeIn()
        default:
            let alert = UIAlertView(title: "Privacy Warning", message: "You have not granted permission for this app to access your Calendar", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
    }
    
    func requestAccessToCalendar() {
        eventStore.requestAccessToEntityType(EKEntityTypeEvent, completion: {
            (accessGranted: Bool, error: NSError!) in
            
            if accessGranted == true {
                dispatch_async(dispatch_get_main_queue(), {
                    self.loadCalendars()
                    self.refreshTableView()
                })
            } else {
                dispatch_async(dispatch_get_main_queue(), {
                    self.needPermissionView.fadeIn()
                })
            }
        })
    }
    
    func loadCalendars() {
        self.calendars = eventStore.calendarsForEntityType(EKEntityTypeEvent) as? [EKCalendar]
        
        var i=0
        
        for aCal in self.calendars!
        {
            if(aCal.calendarIdentifier == self.selectedItemId)
            {
                selectedItemIndex = i
                break
            }
            i++
        }
    }
    
    func refreshTableView() {
        calendarTableView.hidden = false
        calendarTableView.reloadData()
    }
    
    @IBAction func goToSettingsButtonTapped(sender: UIButton) {
        let openSettingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
        UIApplication.sharedApplication().openURL(openSettingsUrl!)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let calendars = self.calendars {
            return calendars.count
        }
        
        return 0
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CalendarSelectorCell") as! UITableViewCell
        
        if let calendars = self.calendars {
            let calendarName = calendars[indexPath.row].title
            cell.textLabel?.text = calendarName
            if calendars[indexPath.row].calendarIdentifier == selectedItemId {
                cell.accessoryType = .Checkmark
            } else {
                cell.accessoryType = .None
            }
        } else {
            cell.textLabel?.text = "Unknown Calendar Name"
        }
        


        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        //Other row is selected - need to deselect it
        if let index = selectedItemIndex {
            let cell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: index, inSection: 0))
            cell?.accessoryType = .None
        }
        
        //update the checkmark for the current row
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        cell?.accessoryType = .Checkmark
        
        selectedItemIndex = indexPath.row
        //        selectedItem = cell?.textLabel!.text
        
        if let calendars = self.calendars {
            selectedItem = calendars[indexPath.row].title
            selectedItemId = calendars[indexPath.row].calendarIdentifier
            println(selectedItemId)
            println(selectedItem)
        }
        
    }
}