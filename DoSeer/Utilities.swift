//
//  Utilities.swift
//  DoSeer
//
//  Created by Bastiaan on 29/07/2015.
//  Copyright (c) 2015 Bastiaan. All rights reserved.
//

import UIKit
import CoreData

// MARK: Helper Functions

func loadActionItems(mode: String, timeslotId: String, contextId: String, projectId: String, filter: Bool, completed: Bool) -> [NSManagedObject] {
    //1
    let appDelegate =
    UIApplication.sharedApplication().delegate as! AppDelegate
    
    let managedContext = appDelegate.managedObjectContext!
    
    //2
    let fetchRequest = NSFetchRequest(entityName:"Action")
    
    // Create a sort descriptor object that sorts on the "title"
    // property of the Core Data object
    let sortDescriptor0 = NSSortDescriptor(key: "completed", ascending: true)
    let sortDescriptor1 = NSSortDescriptor(key: "priority", ascending: false)
    let sortDescriptor2 = NSSortDescriptor(key: "creationdate", ascending: true)
    let sortDescriptor3 = NSSortDescriptor(key: "name", ascending: true, selector: "caseInsensitiveCompare:")
    
    // Set the list of sort descriptors in the fetch request,
    // so it includes the sort descriptor
    fetchRequest.sortDescriptors = [sortDescriptor0, sortDescriptor1, sortDescriptor2, sortDescriptor3]
    
    var subPredicates: [NSPredicate]! = []
    var subPredicate = NSPredicate()
    
    if mode == "inbox" {
        subPredicate = NSPredicate(format: "projectid == nil")
        subPredicates.append(subPredicate)
    }
    else if mode == "now" {
        
        // Select projects within this timeslot
        
        if !timeslotId.isEmpty {
            
            let fetchRequestProject = NSFetchRequest(entityName:"Project")
            let predicateProject = NSPredicate(format: "timeslotid == %@", timeslotId)
            
            fetchRequestProject.predicate = predicateProject
            
            var error: NSError?
            
            let fetchedResults =
            managedContext.executeFetchRequest(fetchRequestProject,
                error: &error) as! [NSManagedObject]?
            
            var projects = [NSManagedObject]()
            
            if let results = fetchedResults {
                projects = results
            } else {
                println("Could not fetch \(error), \(error!.userInfo)")
            }
            
            var projectsA : [String] = [String]()
            
            for resultItem : AnyObject in projects {
                let projectids = resultItem.valueForKey("id") as! String
                projectsA.append(projectids)
            }
            
            // Filter on relevant projects
            
            subPredicate = NSPredicate(format: "projectid IN %@", projectsA)
            subPredicates.append(subPredicate)
        }
        else {
            return [NSManagedObject]()
        }
            
        
        // Exclude other location based contexts
        
        if !contextId.isEmpty {
            let fetchRequestContext = NSFetchRequest(entityName:"Context")
            let predicateContext = NSPredicate(format: "id != %@ and locationlat<>0", contextId)
            
            fetchRequestContext.predicate = predicateContext
            
            var error: NSError?
            
            let fetchedResults =
            managedContext.executeFetchRequest(fetchRequestContext,
                error: &error) as! [NSManagedObject]?
            
            var contexts = [NSManagedObject]()
            
            if let results = fetchedResults {
                contexts = results
            } else {
                println("Could not fetch \(error), \(error!.userInfo)")
            }
            
            var contextsA : [String] = [String]()
            
            for resultItem : AnyObject in contexts {
                let contextids = resultItem.valueForKey("id") as! String
                contextsA.append(contextids)
            }
            
            subPredicate = NSPredicate(format: "NOT contextid IN %@", contextsA)
            subPredicates.append(subPredicate)
        }
    }
    else if !projectId.isEmpty {
        //let projectid = project.valueForKey("id") as! String?
        
        subPredicate = NSPredicate(format: "projectid == %@", projectId)
        subPredicates.append(subPredicate)
    }
    else if !contextId.isEmpty {
        //let contextid = context.valueForKey("id") as! String?
        subPredicate = NSPredicate(format: "contextid == %@", contextId)
        subPredicates.append(subPredicate)
    }
    else if !timeslotId.isEmpty {
        
        // Select projects with this timeslot
        
        let fetchRequestProject = NSFetchRequest(entityName:"Project")
        //let timeslotid = timeslot.valueForKey("id") as! String!
        let predicateProject = NSPredicate(format: "timeslotid == %@", timeslotId)
        
        fetchRequestProject.predicate = predicateProject
        
        var error: NSError?
        
        let fetchedResults =
        managedContext.executeFetchRequest(fetchRequestProject,
            error: &error) as! [NSManagedObject]?
        
        var projects = [NSManagedObject]()
        
        if let results = fetchedResults {
            projects = results
        } else {
            println("Could not fetch \(error), \(error!.userInfo)")
        }
        
        var projectsA : [String] = [String]()
        
        for resultItem : AnyObject in projects {
            let projectids = resultItem.valueForKey("id") as! String
            projectsA.append(projectids)
        }
        
        // Filter on relevant projects
        
        subPredicate = NSPredicate(format: "projectid IN %@", projectsA)
        subPredicates.append(subPredicate)
    }
    
    if (filter) {
        subPredicate = NSPredicate(format: "priority > 0 AND (startdate <= %@ OR startdate == nil)", NSDate())
        subPredicates.append(subPredicate)
    }
    
    
    if (completed) {
        subPredicate = NSPredicate(format: "trash == false")
    }
    else {
        subPredicate = NSPredicate(format: "completed = false AND trash == false")
    }
    subPredicates.append(subPredicate)
    
    // Set the predicate on the fetch request
    //fetchRequest.predicate = predicate
    fetchRequest.predicate = NSCompoundPredicate.andPredicateWithSubpredicates(subPredicates)
    
    
    //3
    var error: NSError?
    
    let fetchedResults =
    managedContext.executeFetchRequest(fetchRequest,
        error: &error) as! [NSManagedObject]?
    
    if let results = fetchedResults {
        return results
    } else {
        println("Could not fetch \(error), \(error!.userInfo)")
    }
    return [NSManagedObject]()
}
