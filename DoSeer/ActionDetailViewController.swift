//
//  ActionDetailViewController.swift
//  next3
//
//  Created by Bastiaan on 25/02/2015.
//  Copyright (c) 2015 Bastiaan. All rights reserved.
//

import UIKit
import CoreData

protocol ActionDetailDelegate{
    func deleteAction(controller:ActionDetailViewController)
}

class ActionDetailViewController: UITableViewController, UITextFieldDelegate, UITextViewDelegate {
    
    var delegate:ActionDetailDelegate! = nil
    var action : NSManagedObject!
    var projectid: String! = nil
    var contextid: String! = nil
    var priority: Int! = 2
    var completed: Bool! = false
    var startdate: NSDate! = nil
    var duedate: NSDate! = nil
    var creationdate: NSDate! = nil
    var timeestimate : NSTimeInterval! = (15 * 60) as NSTimeInterval
    var repeatFrequency : Int! = 0
    var repeatEvery : Int! = 1
    var oldprojectid: String! = nil
    var oldcontextid: String! = nil
    
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var doneButton: UIBarButtonItem!
    @IBOutlet var projectLabel: UILabel!
    @IBOutlet var contextLabel: UILabel!
    @IBOutlet var repeatLabel: UILabel!
    @IBOutlet var timeTextField: UITextField!
    @IBOutlet var creationDateLabel: UILabel!
    
    @IBAction func nameFieldEditingChanged(sender: UITextField) {
        if (!sender.text.isEmpty) {
            doneButton.enabled = true
            sender.textColor = UIColor.blackColor()
        }
        else {
            doneButton.enabled = false
            sender.textColor = UIColor.lightGrayColor()
        }
    }
    
    @IBAction func timeFieldEditing(sender: UITextField) {
        var datePickerView : UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.CountDownTimer
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: Selector("datePickerValueChangedTime:"), forControlEvents: UIControlEvents.ValueChanged)
        if timeestimate != nil {
            datePickerView.countDownDuration = timeestimate
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(0.3 * Double(NSEC_PER_SEC))), dispatch_get_main_queue(), {datePickerView.countDownDuration = datePickerView.countDownDuration})
        }
    }
    
    @IBOutlet var startDateTextField: UITextField!
    @IBAction func startDateFieldEditing(sender: UITextField) {
        var datePickerViewStart  : UIDatePicker = UIDatePicker()
        datePickerViewStart.datePickerMode = UIDatePickerMode.Date
        sender.inputView = datePickerViewStart
        datePickerViewStart.addTarget(self, action: Selector("datePickerValueChangedStart:"), forControlEvents: UIControlEvents.ValueChanged)
        if startdate != nil {
            datePickerViewStart.date = startdate
        }
    }
    @IBOutlet var endDateTextField: UITextField!
    @IBAction func endDateFieldEditing(sender: UITextField) {
        var datePickerViewDue  : UIDatePicker = UIDatePicker()
        datePickerViewDue.datePickerMode = UIDatePickerMode.Date
        sender.inputView = datePickerViewDue
        datePickerViewDue.addTarget(self, action: Selector("datePickerValueChangedDue:"), forControlEvents: UIControlEvents.ValueChanged)
        if duedate != nil {
            datePickerViewDue.date = duedate
        }
    }
    @IBOutlet var costTextField: UITextField!
    @IBOutlet var forecastLabel: UILabel!
    @IBOutlet var notesTextView: UITextView!
    @IBOutlet var startDateLabel: UILabel!
    @IBOutlet var dueDateLabel: UILabel!
    @IBOutlet var completedSegmentedControl: UISegmentedControl!
    
    @IBOutlet var prioritySegmentedControl: UISegmentedControl!
    
    @IBAction func cancelToActionDetailViewController(segue:UIStoryboardSegue) {
    }
    
    func datePickerValueChangedTime(sender: UIDatePicker) {
        let dcf = NSDateComponentsFormatter()
        dcf.zeroFormattingBehavior = .Pad
        dcf.allowedUnits = .CalendarUnitHour | .CalendarUnitMinute
        self.timeTextField.text = dcf.stringFromTimeInterval(sender.countDownDuration)
        timeestimate = sender.countDownDuration
    }
    func datePickerValueChangedStart(sender: UIDatePicker) {
        var dateformatter = NSDateFormatter()
        dateformatter.dateStyle = NSDateFormatterStyle.ShortStyle
        startDateTextField.text = dateformatter.stringFromDate(sender.date)
        startdate = sender.date
    }
    func datePickerValueChangedDue(sender: UIDatePicker) {
        var dateformatter = NSDateFormatter()
        dateformatter.dateStyle = NSDateFormatterStyle.ShortStyle
        endDateTextField.text = dateformatter.stringFromDate(sender.date)
        duedate = sender.date
    }
    
    @IBAction func saveProjectSelection(segue:UIStoryboardSegue) {
        let selectorViewController = segue.sourceViewController as! ProjectSelectorViewController
        if let selectedItem = selectorViewController.selectedItem {
            projectLabel.text = selectedItem
            projectid = selectorViewController.selectedItemId
        }
    }
    
    @IBAction func saveContextSelection(segue:UIStoryboardSegue) {
        let selectorViewController = segue.sourceViewController as! ContextSelectorViewController
        if let selectedItem = selectorViewController.selectedItem {
            contextLabel.text = selectedItem
            contextid = selectorViewController.selectedItemId
        }
    }
    
    @IBAction func saveRepeatSelection(segue:UIStoryboardSegue) {
        let selectorViewController = segue.sourceViewController as! RepeatViewController
        
        selectorViewController
        
        switch (selectorViewController.frequencyLabel.text!) {
        case ("Daily"):
            repeatFrequency = 1
        case ("Weekly"):
            repeatFrequency = 2
        case ("Monthly"):
            repeatFrequency = 3
        case ("Yearly"):
            repeatFrequency = 4
        default:
            repeatFrequency = 0
        }
        repeatEvery = selectorViewController.everyLabel.text?.toInt()
        displayRepeatField()
    }
    
    func displayRepeatField()
    {
        switch (repeatFrequency) {
        case (1):
            repeatLabel.text = NSString(format: "Every %d day(s)", repeatEvery) as String
        case (2):
            repeatLabel.text = NSString(format: "Every %d week(s)", repeatEvery) as String
        case (3):
            repeatLabel.text = NSString(format: "Every %d month(s)", repeatEvery) as String
        case (4):
            repeatLabel.text = NSString(format: "Every %d year(s)", repeatEvery) as String
        default:
            repeatLabel.text = "Never"
        }
    }
    
    func textViewDidBeginEditing(textView: UITextView) {
        if textView.textColor == UIColor.lightGrayColor() {
            textView.text = nil
            textView.textColor = UIColor.blackColor()
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Notes"
            textView.textColor = UIColor.lightGrayColor()
        }
    }
    
    func deleteAction(sender: UIBarButtonItem)
    {
        delegate.deleteAction(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        costTextField.delegate = self
        notesTextView.delegate = self
        
        notesTextView.textColor = UIColor.lightGrayColor()
/*        startDateTextField.textColor = UIColor.lightGrayColor()
        endDateTextField.tintColor = UIColor.lightGrayColor()
        nameTextField.textColor = UIColor.lightGrayColor()
        nameTextField.tintColor = UIColor.lightGrayColor()
*/        
        
        if (action != nil)
        {
            var toolBarItems = NSMutableArray()
            
            //        var systemButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Bookmarks, target: nil, action: nil)
            //        toolBarItems.addObject(systemButton1)
            
            var systemButton2 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
            toolBarItems.addObject(systemButton2)
            
            var systemButton3 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Trash, target: self, action: "deleteAction:")
            toolBarItems.addObject(systemButton3)
            
            var systemButton4 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
            toolBarItems.addObject(systemButton4)
            
            //var systemButton5 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Compose, target: nil, action: nil)
            //toolBarItems.addObject(systemButton5)
            
            self.navigationController?.toolbarHidden = false
            self.setToolbarItems(toolBarItems as [AnyObject], animated: true)

            doneButton.enabled = true
            nameTextField.textColor = UIColor.blackColor()
            self.nameTextField.text = action.valueForKey("name") as! String?
            projectid = action.valueForKey("projectid") as! String?
            contextid = action.valueForKey("contextid") as! String?
            oldprojectid = projectid
            oldcontextid = contextid
            priority = action.valueForKey("priority") as! Int?
            completed = action.valueForKey("completed") as! Bool?
            repeatEvery = action.valueForKey("repeatevery") as! Int?
            repeatFrequency = action.valueForKey("repeatfrequency") as! Int?
            if (repeatFrequency == nil || repeatFrequency == 0)
            {
                repeatFrequency = 0
                repeatEvery = 1
            }
            let cost = action.valueForKey("cost") as! Float?
            if cost != nil {
                self.costTextField.text =  NSString(format: "%.2f", cost!) as String
            }
            duedate = action.valueForKey("duedate") as! NSDate!
            startdate = action.valueForKey("startdate") as! NSDate!
            if startdate != nil {
                self.startDateTextField.text = NSDateFormatter.localizedStringFromDate(startdate, dateStyle: .ShortStyle, timeStyle: .NoStyle)
            }
            if duedate != nil {
                self.endDateTextField.text = NSDateFormatter.localizedStringFromDate(duedate, dateStyle: .ShortStyle, timeStyle: .NoStyle)
            }
            creationdate = action.valueForKey("creationdate") as! NSDate!
            if creationdate != nil {
                self.creationDateLabel.text = NSDateFormatter.localizedStringFromDate(creationdate, dateStyle: .ShortStyle, timeStyle: .NoStyle)
            }

            timeestimate = action.valueForKey("timeestimate") as! NSTimeInterval!
            if timeestimate == nil {
                timeestimate = (15 * 60) as NSTimeInterval
            }
            self.notesTextView.text = action.valueForKey("notes") as! String?
            if !self.notesTextView.text.isEmpty && self.notesTextView.text != "" {
                self.notesTextView.textColor = UIColor.blackColor()
            }
            else
            {
                self.notesTextView.textColor = UIColor.lightGrayColor()
                self.notesTextView.text = "Notes"
            }

            
        }
        else
        {
            //            self.priorityLabel.text = "Select"
        }
        
        if(projectid != nil)
        {
            //1
            let appDelegate =
            UIApplication.sharedApplication().delegate as! AppDelegate
            
            let managedContext = appDelegate.managedObjectContext!
            
            //2
            let fetchRequest = NSFetchRequest(entityName:"Project")
            
            //3
            var error: NSError?
            
            let predicate = NSPredicate(format: "id == %@", projectid)
            
            // Set the predicate on the fetch request
            fetchRequest.predicate = predicate
            
            var items = [NSManagedObject]()
            
            let fetchedResults =
            managedContext.executeFetchRequest(fetchRequest,
                error: &error) as! [NSManagedObject]?
            
            if let results = fetchedResults {
                items = results
            } else {
                println("Could not fetch \(error), \(error!.userInfo)")
            }
            if (items.count==1)
            {
                self.projectLabel.text = items[0].valueForKey("name") as! String?
            }
        }
        
        if(contextid != nil)
        {
            
            //1
            let appDelegate =
            UIApplication.sharedApplication().delegate as! AppDelegate
            
            let managedContext = appDelegate.managedObjectContext!
            
            //2
            let fetchRequest = NSFetchRequest(entityName:"Context")
            
            //3
            var error: NSError?
            
            let predicate = NSPredicate(format: "id == %@", contextid)
            
            // Set the predicate on the fetch request
            fetchRequest.predicate = predicate
            
            var items = [NSManagedObject]()
            
            let fetchedResults =
            managedContext.executeFetchRequest(fetchRequest,
                error: &error) as! [NSManagedObject]?
            
            if let results = fetchedResults {
                items = results
            } else {
                println("Could not fetch \(error), \(error!.userInfo)")
            }
            if (items.count==1)
            {
                self.contextLabel.text = items[0].valueForKey("name") as! String?
            }
        }
        
        if completed == nil || completed  == false {
            completedSegmentedControl.selectedSegmentIndex = 1
        }
        else {
            completedSegmentedControl.selectedSegmentIndex = 0
        }
        
        if timeestimate != nil {
            let dcf = NSDateComponentsFormatter()
            dcf.zeroFormattingBehavior = .Pad
            dcf.allowedUnits = .CalendarUnitHour | .CalendarUnitMinute
            self.timeTextField.text = dcf.stringFromTimeInterval(timeestimate)
        }
        
        switch (priority) {
        case (0):
            prioritySegmentedControl.selectedSegmentIndex = 3
        case (1):
            prioritySegmentedControl.selectedSegmentIndex = 2
        case (2):
            prioritySegmentedControl.selectedSegmentIndex = 1
        case (3):
            prioritySegmentedControl.selectedSegmentIndex = 0
        default:
            prioritySegmentedControl.selectedSegmentIndex = 1
        }
        
        displayRepeatField()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Textfield delegates
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool { // return NO to not change text
        
        switch string {
        case "0","1","2","3","4","5","6","7","8","9":
            return true
        case ".":
            let array = Array(textField.text)
            var decimalCount = 0
            for character in array {
                if character == "." {
                    decimalCount++
                }
            }
            
            if decimalCount == 1 {
                return false
            } else {
                return true
            }
        default:
            let array = Array(string)
            if array.count == 0 {
                return true
            }
            return false
        }
    }
    
    /*
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
    var result = true
    let prospectiveText = (textField.text as NSString).stringByReplacingCharactersInRange(range, withString: string)
    
    if textField == numberField {
    if countElements(string) > 0 {
    let disallowedCharacterSet = NSCharacterSet(charactersInString: "0123456789.-").invertedSet
    let replacementStringIsLegal = string.rangeOfCharacterFromSet(disallowedCharacterSet) == nil
    
    let resultingStringLengthIsLegal = countElements(prospectiveText) <= 6
    
    let scanner = NSScanner(string: prospectiveText)
    let resultingTextIsNumeric = scanner.scanDecimal(nil) && scanner.atEnd
    
    result = replacementStringIsLegal &&
    resultingStringLengthIsLegal &&
    resultingTextIsNumeric
    }
    }
    return result
    }*/
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        if (action != nil) {
            return 12
        }
        else {
            return 11
        }
    }
    
    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as UITableViewCell
    
    // Configure the cell...
    
    return cell
    }
    */
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the specified item to be editable.
    return true
    }
    */
    
    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    if editingStyle == .Delete {
    // Delete the row from the data source
    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    } else if editingStyle == .Insert {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
    }
    */
    
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
    
    }
    */
    
    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the item to be re-orderable.
    return true
    }
    */
    
    
    // MARK: - Navigation
    
    
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        if segue.identifier == "ShowProjectSelector" {
            let itemViewController = segue.destinationViewController as! ProjectSelectorViewController
            if action != nil && action.valueForKey("projectid") != nil {
                itemViewController.selectedItemId = action.valueForKey("projectid") as! String?
            }
        }
        if segue.identifier == "ShowContextSelector" {
            let itemViewController = segue.destinationViewController as! ContextSelectorViewController
            if action != nil && action.valueForKey("contextid") != nil {
                itemViewController.selectedItemId = action.valueForKey("contextid") as! String?
            }
        }
        if segue.identifier == "ShowRepeatSelector" {
            let itemViewController = segue.destinationViewController as! RepeatViewController
            itemViewController.repeatFrequency = repeatFrequency
            itemViewController.repeatEvery = repeatEvery
        }
    }
    
    
}
