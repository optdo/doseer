//
//  ContextDetailViewController.swift
//  next3
//
//  Created by Bastiaan on 25/02/2015.
//  Copyright (c) 2015 Bastiaan. All rights reserved.
//

import UIKit
import MapKit
import CoreData

protocol ContextDetailDelegate{
    func deleteContext(controller:ContextDetailViewController)
}

class ContextDetailViewController: UITableViewController {
    
    var delegate:ContextDetailDelegate! = nil
    
    var context : NSManagedObject!
    var location: CLLocation?
    var locationInfo : String?
    
    @IBOutlet var locationLabel: UILabel!
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var doneButton: UIBarButtonItem!
    
    @IBAction func cancelToContextDetailViewController(segue:UIStoryboardSegue) {
        location = nil
        locationInfo = nil
        locationLabel.text = "Not set"
    }
    
    @IBAction func saveContextLocation(segue:UIStoryboardSegue) {
        let selectorViewController = segue.sourceViewController as! ContextLocationViewController
        
        if (selectorViewController.selectedLocation != nil) {
            location = selectorViewController.selectedLocation
            locationInfo = selectorViewController.selectedLocationInfo
            locationLabel.text = locationInfo
        }
        else {
            location = nil
            locationInfo = nil
            locationLabel.text = "Not set"
        }
        
    }
    
    @IBAction func nameFieldEditing(sender: UITextField) {
        if (!sender.text.isEmpty) {
            doneButton.enabled = true
        }
        else {
            doneButton.enabled = false
        }
        
    }
    
    func deleteContext(sender: UIBarButtonItem)
    {
        delegate.deleteContext(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (context != nil)
        {
            var toolBarItems = NSMutableArray()
            
            //        var systemButton1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Bookmarks, target: nil, action: nil)
            //        toolBarItems.addObject(systemButton1)
            
            var systemButton2 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
            toolBarItems.addObject(systemButton2)
            
            var systemButton3 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Trash, target: self, action: "deleteContext:")
            toolBarItems.addObject(systemButton3)
            
            var systemButton4 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
            toolBarItems.addObject(systemButton4)
            
            //var systemButton5 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Compose, target: nil, action: nil)
            //toolBarItems.addObject(systemButton5)
            
            self.navigationController?.toolbarHidden = false
            self.setToolbarItems(toolBarItems as [AnyObject], animated: true)
            
            self.nameTextField.text = context.valueForKey("name") as! String?
            
            
            locationInfo = context.valueForKey("location") as! String?
            
            if locationInfo != nil {
                
                var loclat: Double = 0.0
                var loclong: Double = 0.0
                if let lat = context.valueForKey("locationlat") as? Double {
                    loclat = lat
                }
                
                if let long = context.valueForKey("locationlong") as? Double {
                    loclong = long
                }
                
                if (loclat != 0 && loclong != 0)
                {
                    var curCLloc: CLLocation =  CLLocation(latitude: loclat, longitude: loclong)
                    
                    location = curCLloc
                    
                    //locationLabel.text = String(format: "%.4f", loclat) + ", " + String(format: "%.4f",loclong)
                    locationLabel.text = locationInfo
                }
            }
            doneButton.enabled = true
        }
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // Return the number of sections.
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Return the number of rows in the section.
        return 2
    }
    
    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath) as UITableViewCell
    
    // Configure the cell...
    
    return cell
    }
    */
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the specified item to be editable.
    return true
    }
    */
    
    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    if editingStyle == .Delete {
    // Delete the row from the data source
    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    } else if editingStyle == .Insert {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
    }
    */
    
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
    
    }
    */
    
    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the item to be re-orderable.
    return true
    }
    */
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        if segue.identifier == "ShowContextLocation" {
            let viewController = segue.destinationViewController as! ContextLocationViewController
            if (location != nil) {
                viewController.selectedLocation = location
            }
        }
        
    }
    
    
}
